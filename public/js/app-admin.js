var appAdmin = angular.module('appAdmin', ['ngAnimate', 'ui.bootstrap', 'moduleGlobal']);

appAdmin
  .config([function(){
    // any configuration goes here
  }])

  .controller('controllerAdmin', function($scope, $log, $modal, $http) {
    $scope.pageName = "admin";

    
    $http.get('/admin/users/')
      .then(function(res) {
        $scope.users = res.data;
      });

    $http.get('/admin/classes/')
      .then(function(res) {
        $scope.classes = res.data;
      });

    $http.get('/admin/courses/')
      .then(function(res) {
        $scope.courses = res.data;
      });




    /********************* SESSION STATE ********************/
    $scope.currentSelection = {
      userName:"",
      userId:"",
      userPrimaryRole:"",
      userOccupation:"",

      courseName:"",
      courseDepartment:"",
      courseId:"",

      classId:"",

      filteredUserIds:[],
      filteredCourseIds:[],
      filteredClassIds:[]
    };


    /********************* FUNCTIONS ********************/
    $scope.primaryRoleOf = function(user) {
      var studentClasses = 0;
      var professorClasses = 0;

      // TODO

      return "Student";
    };

    $scope.createUser = function() {
      $scope.users['0000000'] = {
        name: ' New User',
        id: '0000000',
        occupation: 'Unkown',
        associations: {
          courseIds: [
          ],
          classIds: [
          ]
        }
      };
    };

    $scope.editUser = function(user) {
      var modalInstance = $modal.open({
        templateUrl: '/directive-templates/admin/template-edit-user.html',
        controller: function ($scope, $modalInstance) {
          // INITIALIZE FIELDS WITH DATA

          $scope.user = user;

          $scope.keyPressed = function(keyCode) {
            if(keyCode == 13) {
              $scope.ok();
            }
          };
          $scope.ok = function () {
            $modalInstance.close($scope.user); // UPDATE VALUE
          };
          $scope.cancel = function () {
            $modalInstance.dismiss('cancel'); // CLOSE WITHOUT UPDATING
          };
        }
      });
      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
      }, function () {
      });
    };

    $scope.createCourse = function() {
      $scope.courses['0000'] = {
        name: 'New Course',
        department: '---',
        number: '0000',
        associations: {
          classIds: [
          ]
        }
      }
    };

    $scope.editCourse = function(course) {
      var modalInstance = $modal.open({
        templateUrl: '/directive-templates/admin/template-edit-course.html',
        controller: function ($scope, $modalInstance) {
          // INITIALIZE FIELDS WITH DATA

          $scope.course = course;

          $scope.keyPressed = function(keyCode) {
            if(keyCode == 13) {
              $scope.ok();
            }
          };
          $scope.ok = function () {
            $modalInstance.close($scope.course); // UPDATE VALUE
          };
          $scope.cancel = function () {
            $modalInstance.dismiss('cancel'); // CLOSE WITHOUT UPDATING
          };
        }
      });
      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
      }, function () {
      });
    };

    $scope.createClass = function() {
      $scope.classes['000000'] = {
        courseId: '0000',
        id: '000000',
        associations: {
          userIds: [
          ],
          courseId: '0000'
        }
      }
    };

    $scope.editClass = function(clazz) {
      var modalInstance = $modal.open({
        templateUrl: '/directive-templates/admin/template-edit-class.html',
        controller: function ($scope, $modalInstance) {
          // INITIALIZE FIELDS WITH DATA

          $scope.clazz = clazz;

          $scope.keyPressed = function(keyCode) {
            if(keyCode == 13) {
              $scope.ok();
            }
          };
          $scope.ok = function () {
            $modalInstance.close($scope.clazz); // UPDATE VALUE
          };
          $scope.cancel = function () {
            $modalInstance.dismiss('cancel'); // CLOSE WITHOUT UPDATING
          };
        }
      });
      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
      }, function () {
      });
    };

  })

  /********************* FILTERS ********************/
  .filter('filterBringNewUserToFront', function() {
      return   function(users) {
        var newUserIndex = -1;
        for(var i = 0; i < users.length; i++) {
          if(users[i].id == '0000000') {
            newUserIndex = i;
          }
        }

        while(newUserIndex > 0) {
          var temp = users[newUserIndex - 1];
          users[newUserIndex - 1] = users[newUserIndex];
          users[newUserIndex] = temp;
          newUserIndex--;
        }
        return users;
      }
  })

  .filter('filterUsersByFields', function($log) {
      return   function(users, scope) {

        var filtered = [];
        scope.filteredUserIds = [];

        angular.forEach(users, function(user) {
          if(user.name.toLowerCase().indexOf(scope.currentSelection.userName.toLowerCase()) !== -1 &&
               user.id.toString().toLowerCase().indexOf(scope.currentSelection.userId.toString().toLowerCase()) !== -1 &&
               scope.primaryRoleOf(user.id).toLowerCase().indexOf(scope.currentSelection.userPrimaryRole.toLowerCase()) !== -1 &&
               user.occupation.toLowerCase().indexOf(scope.currentSelection.userOccupation.toLowerCase()) !== -1) {
            filtered.push(user);
            scope.filteredUserIds.push(user.id);
          }
        });

        return filtered;

      };
  })

  .filter('filterUsersByOtherTables', function($log) {
      return   function(users, scope) {

        var filtered = [];

        angular.forEach(users, function(user) {
          angular.forEach(scope.filteredClassIds, function(clazzId) {
            if(scope.classes[clazzId].associations.userIds.indexOf(user.id) != -1 && filtered.indexOf(user) == -1) {
              filtered.push(user);
            }
          });
        });

        return filtered;

      };
  })

  .filter('filterBringNewCourseToFront', function() {
      return   function(courses) {
        var newCourseIndex = -1;
        for(var i = 0; i < courses.length; i++) {
          if(courses[i].number == '0000') {
            newCourseIndex = i;
          }
        }

        while(newCourseIndex > 0) {
          var temp = courses[newCourseIndex - 1];
          courses[newCourseIndex - 1] = courses[newCourseIndex];
          courses[newCourseIndex] = temp;
          newCourseIndex--;
        }
        return courses;
      }
  })

  .filter('filterCoursesByFields', function($log) {
      return   function(courses, scope) {

        var filtered = [];
        scope.filteredCourseIds = [];

        angular.forEach(courses, function(course) {
          if(course.name.toLowerCase().indexOf(scope.currentSelection.courseName.toLowerCase()) !== -1 &&
               course.department.toLowerCase().indexOf(scope.currentSelection.courseDepartment.toLowerCase()) !== -1 &&
               course.number.toLowerCase().indexOf(scope.currentSelection.courseId.toLowerCase()) !== -1) {
            filtered.push(course);
            scope.filteredCourseIds.push(course.number);
          }
        });

        return filtered;

      };
  })

  .filter('filterCoursesByOtherTables', function($log) {
      return   function(courses, scope) {

        var filtered = [];

        angular.forEach(courses, function(course) {
          angular.forEach(scope.filteredClassIds, function(clazzId) {
            if(course.number == scope.classes[clazzId].associations.courseId && filtered.indexOf(course) == -1) {
              filtered.push(course);
            }
          });
        });

        return filtered;

      };
  })

  .filter('filterBringNewClassToFront', function() {
      return   function(classes) {
        var newClassIndex = -1;
        for(var i = 0; i < classes.length; i++) {
          if(classes[i].id == '000000') {
            newClassIndex = i;
          }
        }

        while(newClassIndex > 0) {
          var temp = classes[newClassIndex - 1];
          classes[newClassIndex - 1] = classes[newClassIndex];
          classes[newClassIndex] = temp;
          newClassIndex--;
        }
        return classes;
      }
  })

  .filter('filterClassesByFields', function($log) {
      return   function(classes, scope) {

        var filtered = [];
        scope.filteredClassIds = [];

        angular.forEach(classes, function(clazz) {
          if(clazz.id.toString().toLowerCase().indexOf(scope.currentSelection.classId.toLowerCase()) !== -1) {
            filtered.push(clazz);
            scope.filteredClassIds.push(clazz.id);
          }
        });

        return filtered;

      };
  })


  .filter('filterClassesByOtherTables', function($log) {
      return   function(classes, scope) {

        var filtered = [];

        angular.forEach(classes, function(clazz) {

          angular.forEach(scope.filteredCourseIds, function(courseId) {
            if(clazz.courseId == courseId && filtered.indexOf(clazz) == -1) {
              filtered.push(clazz);
            }
          });

          angular.forEach(scope.filteredUserIds, function(userId) {
            if(clazz.associations.userIds.indexOf(userId) != -1 && filtered.indexOf(clazz) == -1) {
               filtered.push(clazz);
            }
          });

        });

        return filtered;

      };
  })

;
