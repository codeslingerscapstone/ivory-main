angular.module('moduleGlobal')
  .controller('controllerPageHeader', function($scope){
    $scope.brandName = "Ivory";
  })
  .directive('pageHeader', function() {
    return {
      controller:'controllerPageHeader',
      templateUrl:'/directive-templates/template-page-header.html',
      replace:true
    };
  });
