angular.module('moduleGlobal')
  .controller('controllerFooter', function($scope){
    // set up scope here
  })
  .directive('footer', function() {
    return {
      controller:'controllerFooter',
      templateUrl:'/directive-templates/template-footer.html',
      replace:true
    };
  });
