angular.module('appDashboard')
  .controller('controllerProfCourseView', function($scope, $http){
    
    
    $scope.getCurrentTool = function() {
      return $scope.tool;
    };
    $scope.setCurrentTool = function (toolName) {
      $scope.tool = toolName;
    };

    
  })
  .directive('profCourseView', function() {
    return {
      controller:'controllerProfCourseView',
      templateUrl:'/directive-templates/template-prof-course-view.html',
      scope: {
        tool:'=',
        currClass:'=curr',
        allClasses:'@all',
        userName:'@user'
      }
    };
  })
;
