angular.module('moduleGlobal')
  .controller('controllerNavigationBar', function($scope){
    // set up scope here
  })
  .directive('navigationBar', function() {
    return {
      controller:'controllerNavigationBar',
      templateUrl:'/directive-templates/template-navigation-bar.html',
      replace:true
    };
  });
