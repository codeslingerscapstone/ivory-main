angular.module('moduleGlobal')
  .directive('angularObject', function($log) {
    return {
      scope: {
        ngData: '=',
        data: '='
      },
      link: function(scope, elem, attrs) {
        attrs.$set('data', scope.ngData);
      }
    };
  });
//#toolbar=1&amp;navpanes=0&amp;scrollbar=1&amp;page=1&amp;view=FitH