angular.module('appSignup')
  .controller('controllerSignupPanel', function($scope){
    // set up scope here
  })
  .directive('signupPanel', function() {
    return {
      controller:'controllerSignupPanel',
      templateUrl:'/directive-templates/template-signup-panel.html',
      replace:true
    };
  });
