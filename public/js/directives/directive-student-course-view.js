angular.module('appDashboard')
  .controller('controllerStudentCourseView', function($scope, $http){
    
    $scope.gradeNot = [];
    $scope.attendNot = [];
    $scope.assignNot = [];
    $scope.discussNot = [];

    


    $http.get('/notifications/')
     .then(function(res) {
      $scope.notifications = res.data;
     }).then(function() {

      for(notify in $scope.notifications) {
        switch($scope.notifications[notify].typeId)
        {
          case 1:
            if($scope.notifications[notify].classId == $scope.currClass) {
              $scope.gradeNot.push($scope.notifications[notify]);
            }
            
            break;
          case 2:
            if($scope.notifications[notify].classId == $scope.currClass) {
              $scope.attendNot.push($scope.notifications[notify]); 
            }
          
            break;
          case 3:
            if($scope.notifications[notify].classId == $scope.currClass) {
              $scope.assignNot.push($scope.notifications[notify]);
            }
          
            break;
          case 4:
            if($scope.notifications[notify].classId == $scope.currClass) { 
              $scope.discussNot.push($scope.notifications[notify]);
            }
            break;
        }
      }
    });

    $scope.clearNotifications = function(classId, typeId) {

      switch(typeId) {
        case 1:
          if($scope.gradeNot.length != 0) {
            $http.delete('/notifications/class/' + classId + '/type/' + 1);
          }
          $scope.gradeNot = [];
          break;
        case 2:
          if($scope.attendNot.length != 0) {
            $http.delete('/notifications/class/' + classId + '/type/' + 2);
          }
          $scope.attendNot = [];
          break;
        case 3:
          if($scope.assignNot.length != 0) {
            $http.delete('/notifications/class/' + classId + '/type/' + 3);
          }
          $scope.assignNot = [];
          break;
        case 4:
          if($scope.discussNot.length != 0) {
            $http.delete('/notifications/class/' + classId + '/type/' + 4);
          }
          $scope.discussNot = [];
          break;
      }

    };
    



    $scope.getCurrentTool = function() {
      return $scope.tool;
    };
    $scope.setCurrentTool = function (toolName) {
      $scope.tool = toolName;
    };
  })
  .directive('studentCourseView', function() {
    return {
      controller:'controllerStudentCourseView',
      templateUrl:'directive-templates/template-student-course-view.html',
      scope: {
        tool:'=',
        currClass:'=curr',
        allClasses:'@all',
        userName:'@user'
      }
    };
  })
;
