angular.module('appDashboard')
  .controller('controllerProfAttendanceTool', function($scope,$http,$filter, $log){
  	//$scope.allClasses = JSON.parse($scope.allClasses);  //JSONifying the string
  	//$scope.allUsers = JSON.parse($scope.allUsers);
  	$scope.index = 0;
    $scope.newAttend = false;
    $scope.updateAttend = false;
    $scope.attendances = [];
    $scope.selectedItem = {};
    $scope.updateClicked = {};


    $http.get('/api/classes/prof/allUsers')
      .then(function(res) {
        $scope.allUsers = res.data;
    }).then(function(res) {
      var allClasses = JSON.parse($scope.allClasses); 
      //For all classes the prof is teaching
      for(section in allClasses) {
        //grab the attendance data for the class
        for(currClass in $scope.allUsers) {
          if($scope.allUsers[currClass].classID == allClasses[section].classId) {
            var userIDs = $scope.allUsers[currClass].IDs;

          }
        }

        for(ID in userIDs) {
          $scope.url = '/api/attendance/class/' + allClasses[section].classId + '/user/' + userIDs[ID];
          $http.get($scope.url)
            .then(function(res) {
              //store the data in the array
              $scope.attendances.push(res.data);
              //$scope.index++;
              $scope.set = true;
            });
            if($scope.selectedItem[userIDs[ID]] != 'P') {
              $scope.selectedItem[userIDs[ID]] = 'P';
            }
        }
      }
    });


    $scope.getUserIDs = function(classId) {
      for(currClass in $scope.allUsers) {
          if($scope.allUsers[currClass].classID == classId) {
            return $scope.allUsers[currClass].IDs;
          }
        }
    };

    //get attendace for a specific class
    $scope.getAttendance = function(classId) {
      if($scope.set == true) {
        var results = [];
        for(dayStudent in $scope.attendances) {
          if($scope.attendances[dayStudent].length > 0) {
            if($scope.attendances[dayStudent][0].classId == classId) {
              results.push($scope.attendances[dayStudent]);
            }
          }
        }

        return results;
      }
    };

    $scope.postAttendance = function() {
      for(currClass in $scope.allUsers) {
        if($scope.allUsers[currClass].classID == $scope.curr) {
          var IDs = $scope.allUsers[currClass].IDs;
        }
      }

      for(ID in IDs) {
          $scope.postUrl = '/users/attendance/' + $scope.curr + '/' + IDs[ID] + '/' + $scope.selectedItem[IDs[ID]] + '/' + $filter('date')($scope.dt,'yyyy-MM-dd');
          $http.post($scope.postUrl);
          $scope.newAttend = false;
          $scope.selectedItem[IDs[ID]] = 'P';
      }
    };

    $scope.updateAttendance = function(ID, updatedAttendance) {
      $scope.putUrl = '/users/attendance/' + $scope.curr + '/' + ID + '/' + updatedAttendance + '/' + $filter('date')($scope.dt,'yyyy-MM-dd');
      $http.put($scope.putUrl);
      $scope.updateClicked[ID] = true;
    };

    $scope.today = function() {
      $scope.dt = new Date();
    };
    $scope.today();

    $scope.showWeeks = false;
    $scope.toggleWeeks = function () {
      $scope.showWeeks = ! $scope.showWeeks;
    };

    $scope.clear = function () {
      $scope.dt = null;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
      return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    };

    $scope.toggleMin = function() {
      $scope.minDate = ( $scope.minDate ) ? null : new Date();
    };

    $scope.open = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened = true;
    };

    $scope.getThumbnailStatusClasses = function(attendanceStatus) {
      if(attendanceStatus == "P")
        return "bg-success";
      else if(attendanceStatus == "UL" || attendanceStatus == "EL")
        return "bg-warning";
      else if(attendanceStatus == "UA" || attendanceStatus == "EA")
        return "bg-danger"
    };
    $scope.cycleAttendanceStatus = function(student) {
      if(student.status == "P")
        student.status = "UL";
      else if(student.status == "UL" || student.status == "EL")
        student.status = "UA";
      else if(student.status == "UA" || student.status == "EA")
        student.status = "P";
    }
    $scope.getReadableStatus = function(statusSymbol) {
      if(statusSymbol == "P")
        return "Present";
      else if(statusSymbol == "EL")
        return "Late (excused)";
      else if(statusSymbol == "UL")
        return "Late";
      else if(statusSymbol == "EA")
        return "Absent (excused)";
      else if(statusSymbol == "UA")
        return "Absent";
    }

    $scope.dateOptions = {
      'year-format': "'yy'",
      'starting-day': 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.format = $scope.formats[0];


    $scope.items = [
      'P',
      'UL',
      'EL',
      'UA',
      'EA'
    ];
  })
  .directive('profAttendanceTool', function() {
    return {
      controller:'controllerProfAttendanceTool',
      templateUrl:'/directive-templates/tools-prof/template-prof-attendance-tool.html',
      scope:{
        curr:'@',
        allClasses:'@all',
        //allUsers: '@allusers'
      }
    };
  })
  .filter('attendstatus', function() {
    return function(input) {
        switch(input) {
          case 'P':
            return 'Present';
            break;
          case 'UA':
            return 'Unexcused Absent';
            break;
          case 'UL':
            return 'Unexcued Late';
            break;
          case 'EA':
            return 'Excused Absent';
            break;
          case 'EL':
            return 'Excused Late';
            break;
        }
      }
  })
;