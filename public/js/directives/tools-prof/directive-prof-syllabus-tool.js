angular.module('appDashboard')
  .controller('controllerProfSyllabusTool', function($scope, $http){

      $scope.allClasses = JSON.parse($scope.allClasses);
      $scope.syllabi = [];
      $scope.index = 0;
      $scope.editSyl = false;
      $scope.currSyllabus;


      for(curr in $scope.allClasses) {
        $scope.url = /syllabus/ + $scope.allClasses[curr].classId;
    
        $http.get($scope.url)
          .then(function(res) {
            if(res.data != "") {
              $scope.syllabi.push(res.data);
              $scope.index++;
            }
        });
      }

      $scope.showSyllabus = function(currentId) {
        for(syllabus in $scope.syllabi) {
          if($scope.syllabi[syllabus].classID == currentId) {
            $scope.currSyllabus = $scope.syllabi[syllabus]
            return $scope.syllabi[syllabus];
          }
        } 
      }

      $scope.submitSyllabus = function(text) {
        $scope.editSyl = false;

        var fd = new FormData();
        var url = '/api/syllabus/class/' + $scope.curr;

        fd.append('text', text);
        fd.append('uploadedFile', $scope.uploadedFile);

        $http.post(url, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        });
  

      }

  })
  .directive('profSyllabusTool', function() {
    return {
      controller:'controllerProfSyllabusTool',
      templateUrl:'/directive-templates/tools-prof/template-prof-syllabus-tool.html',
      scope:{
        curr:'@',
        allClasses:'@all'
      }
    };
  })
  .directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
  }])
;
