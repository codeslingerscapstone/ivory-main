angular.module('appDashboard')
  .controller('controllerProfGradesTool', function($scope, $http, $modal, $log){
  	$scope.assignments = [];
    $scope.grades = {};
    $scope.allClasses = JSON.parse($scope.allClasses);
    $scope.clickedVar = {};
    $scope.index = 0;
    $scope.showDoc = false;


    //have to iterate through this
    for(curr in $scope.allClasses) {

      $scope.url = '/api/assignments/class/' + $scope.allClasses[curr].classId;
      $http.get($scope.url)
        .then(function(res) {
          if(res.data != "") {
            $scope.assignments.push(res.data);
          }
      });
    }


  	$http.get('/api/classes/prof/allUsers')
      .then(function(res) {
        $scope.allUsers = res.data;
    }).then(function(res) {
      for(classObj in $scope.allUsers) {
        $scope.grades[$scope.allUsers[classObj].classID] = [];
        for(ID in $scope.allUsers[classObj].IDs) {
          $scope.url = '/grades/' + $scope.allUsers[classObj].classID + '/' + $scope.allUsers[classObj].IDs[ID];
          $http.get($scope.url)
            .then(function(res) {
              $scope.grades[res.data[0].classID].push(res.data);
            });

        }
      }
    });

    $scope.getAssigns = function(currentId) {
      for(assigns in $scope.assignments) {
        if($scope.assignments[assigns][0] != undefined) {
          if($scope.assignments[assigns][0].ClassId == currentId) {
            return $scope.assignments[assigns];
          }
        }
      }
    }

    $scope.getCurrClass = function(curr) {
		for(user in $scope.allUsers) {
		    if($scope.allUsers[user].classID == curr) {
		    	return $scope.allUsers[user].IDs;
		    }
		  }
	  }

    $scope.currGrades = function(classId, currAssignment) {
      $scope.gradesByAssign = [];
      for(user in $scope.grades[classId]) {
        for(assign in $scope.grades[classId][user]) {
          if($scope.grades[classId][user][assign].title == currAssignment) {
            if($scope.grades[classId][user][assign].grade == "N/A") {
              $scope.grades[classId][user][assign].grade = "";
            }
            $scope.gradesByAssign.push($scope.grades[classId][user][assign]);
          }
        }
      }

      return $scope.gradesByAssign;
    }

    $scope.clicked = function(title) {
      $scope.clickedVar[title] = true;
    }

    $scope.loseFocus = function(title) {
      $scope.clickedVar[title] = false;
    }

    $scope.postGrades = function(curr, currUser, assignId, title, grade) {
      $scope.putUrl = '/grades/' + curr + '/' + currUser + '/' + assignId + '/' + grade;
      $http.put($scope.putUrl);
      $scope.clickedVar[title] = false;
    }




    //Modal methods
    $scope.open = function(id, assignment) {
      $http.get("/documents/" + id)
       .then(function(res) {
          $scope.showDoc = false;
          $scope.studentDoc = res.data;
       }).then(function() {

        var doc = $scope.studentDoc;

        if($scope.studentDoc.filePath != null) {
          // DETERMINE FILE TYPE
          var docExtension = $scope.studentDoc.filePath.split(".")[$scope.studentDoc.filePath.split(".").length - 1];
          var codeFileExtensions = [
            'c', 'cpp', 'h', 'hpp', 'java', 'js', 'py', 'html'
          ];

          // VIEW PDF
          if(docExtension == 'pdf') {
            var modalInstance = $modal.open({
              windowClass: 'view-document',
              templateUrl: '/directive-templates/document-views/template-view-document-pdf.html',

              controller: function ($scope, $modalInstance) {
                // INITIALIZE FIELDS WITH DATA
                $scope.assignment = assignment;
                $scope.doc = doc;

                $scope.keyPressed = function(keyCode) {
                  if(keyCode == 13) {
                    $scope.ok();
                  }
                };
                $scope.ok = function () {
                  $modalInstance.close($scope.user); // UPDATE VALUE
                };
                $scope.cancel = function () {
                  $modalInstance.dismiss('cancel'); // CLOSE WITHOUT UPDATING
                };
              }
            });
          // VIEW CODE
          } else if(codeFileExtensions.indexOf(docExtension) != -1) {
            var modalInstance = $modal.open({
              windowClass: 'view-document',
              templateUrl: '/directive-templates/document-views/template-view-document-code.html',
              controller: function ($scope, $modalInstance) {
                // INITIALIZE FIELDS WITH DATA
                $scope.assignment = assignment;
                $scope.doc = doc;

                $scope.keyPressed = function(keyCode) {
                  if(keyCode == 13) {
                    $scope.ok();
                  }
                };
                $scope.ok = function () {
                  $modalInstance.close($scope.user); // UPDATE VALUE
                };
                $scope.cancel = function () {
                  $modalInstance.dismiss('cancel'); // CLOSE WITHOUT UPDATING
                };
              }
            });
            modalInstance.opened.then(function () {
              hljs.initHighlightingOnLoad();
            }, function () {
            });

          } else {
            //display text
          }
        }
      });
    }

        
        /*
        $scope.tempId = id;
        var modalInstance = $modal.open({
          templateUrl: 'documentWindow.html',
          controller: ModalInstanceCtrl,
          resolve: {
            studentDoc: function() {
              return $scope.studentDoc;
            }
          }
        });

        modalInstance.result.then(function (selectedItem) {
          $scope.selected = selectedItem;
        }, function () {
          
        });
      });     
    };

    // Please note that $modalInstance represents a modal window (instance) dependency.
    // It is not the same as the $modal service used above.

    var ModalInstanceCtrl = function ($scope, $modalInstance, studentDoc) {

      $scope.studentDoc = studentDoc;


      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
        $scope.showDoc = false;
      };
    };

*/

  })
  .directive('profGradesTool', function() {
    return {
      controller:'controllerProfGradesTool',
      templateUrl:'/directive-templates/tools-prof/template-prof-grades-tool.html',
      scope:{
        curr:'@',
        allClasses:'@all',
        //allUsers: '@allusers'
      }
    };
  })
;
