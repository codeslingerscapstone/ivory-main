angular.module('appDashboard')
  .controller('controllerProfAssignmentsTool', function($scope,$http, $filter){
    $scope.assignments = [];
    $scope.allClasses = JSON.parse($scope.allClasses);
    $scope.HDD = 256;
    $scope.mem = 512;
    $scope.seconds = 1;


    //have to iterate through this
    for(curr in $scope.allClasses) {
      $scope.url = '/api/assignments/class/' + $scope.allClasses[curr].classId;
      $http.get($scope.url)
        .then(function(res) {
          if(res.data != "") {
            $scope.assignments.push(res.data);
          }
      });
    }
    
    $scope.dt;
    $scope.defView = true;

    $scope.getAssigns = function(currentId) {
      for(assigns in $scope.assignments) {
        if($scope.assignments[assigns][0].ClassId == currentId) {
          return $scope.assignments[assigns];
        }
      }
    }

    $scope.newAssign = function() {
      $scope.defView = false;
    }

    $scope.submitAssign = function(currentId) {

      var assign = {
        ClassId: currentId,
        title: $scope.newAssignName,
        due: $scope.dt,
        tokenNum: -1,
        tokenType: 'total',
        submission: false,
        promptText: $scope.prompt
      };

      for(assigns in $scope.assignments) {
        if($scope.assignments[assigns][0].ClassId == currentId) {
          $scope.assignments[assigns].push(assign);
        }
      }
      

      var url = '/api/assignments/class/' + currentId;
      var fd = new FormData();

      console.log($scope.numTokens);
      console.log($scope.selectedType);

      fd.append('title', $scope.newAssignName);
      fd.append('due', $filter('date')($scope.dt, 'yyyy-MM-dd'));

      if($scope.submission == true && $scope.tokenBool == true) {
        fd.append('tokenNum', $scope.numTokens);
        fd.append('tokenType', $scope.selectedType);
      } else {
        fd.append('tokenNum', -1);
        fd.append('tokenType', 'total');
      }

      if($scope.code == true) {
        $scope.submission = true;
        fd.append('programLang', $scope.selectedLanguage);
        fd.append('HDD', $scope.HDD);
        fd.append('memory', $scope.mem);
        fd.append('runTime', $scope.seconds);
        fd.append('accuracy', $scope.accuracy);
        if($scope.inputBool == true) {
          fd.append('inputFile', $scope.codeInput);
          fd.append('outputFile', $scope.correctOutput);
        }
        else if($scope.inputBool == undefined) {
          $scope.inputBool = false;
        }
        fd.append('inputBool', $scope.inputBool);
      }
      fd.append('code', $scope.code);
      
      if($scope.submission == undefined) {
        $scope.submission = false;
      }
      fd.append('submission', $scope.submission);
      fd.append('promptText', $scope.prompt);
      fd.append('uploadedFile', $scope.uploadedFile);

      $http.post(url, fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined}
      });

      $scope.defView = true;


    }

    $scope.open = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened = true;
    };

    $scope.dateOptions = {
      'year-format': "'yy'",
      'starting-day': 1
    };

    $scope.types = [
      'hourly',
      'daily',
      'weekly',
      'total'
    ];

    $scope.langs = [
      'Java',
      'C++',
      'Python',
      'Javascript'
    ];


  })
  .directive('profAssignmentsTool', function() {
    return {
      controller:'controllerProfAssignmentsTool',
      templateUrl:'/directive-templates/tools-prof/template-prof-assignments-tool.html',
      scope:{
        curr:'@',
        allClasses:'@all',
        //allUsers: '@allusers'
      }
    };
  }).directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
  }])
;
