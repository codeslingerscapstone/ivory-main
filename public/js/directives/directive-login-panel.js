angular.module('appLogin')
  .controller('controllerLoginPanel', function($scope){
    // set up scope here
  })
  .directive('loginPanel', function() {
    return {
      controller:'controllerLoginPanel',
      templateUrl:'/directive-templates/template-login-panel.html',
      replace:false
    };
  });
