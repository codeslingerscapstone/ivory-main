angular.module('appDashboard')
  .controller('controllerAttendanceGrid', function($scope){
    $scope.attendanceClicked = function(student) {
      $scope.cycleAttendanceStatus(student);
      $scope.updateAttendance(student.userId, student.status);
    };
  })
  .directive('attendanceGrid', function() {
    return {
      controller:'controllerAttendanceGrid',
      templateUrl:'/directive-templates/template-attendance-grid.html',
      replace:true
    };
  });
