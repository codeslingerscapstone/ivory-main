angular.module('appDashboard')
  .controller('controllerStudentDiscussionTool', function($scope, $http){

    $scope.commentClicked = {}
    $scope.replyClick = {};
    $scope.posts = [];
    $scope.comments = {};

    Date.prototype.timestamp = function() {         
                                
        var yyyy = this.getFullYear().toString();                                    
        var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based         
        var dd  = this.getDate().toString();             
                            
        return yyyy + '-' + (mm[1]?mm:"0"+mm[0]) + '-' + (dd[1]?dd:"0"+dd[0]) + ' ' + this.getHours() + ':' + this.getMinutes() + ':' + this.getSeconds() + '-05';
    };  

    console.log($scope.userName);

    $scope.url = "/api/posts/" + $scope.curr;
    $http.get($scope.url)
     .then(function(res) {
        for(post in res.data) {
          $scope.posts.push(res.data[post]);
        }
     }).then(function(res) {
        for(post in $scope.posts) {
          $scope.url = "/posts/comments/" + $scope.posts[post].id;
          $http.get($scope.url)
           .then(function(res) {
              $scope.comments[res.data[0].postID] = res.data;
           });    
        }
     });



    $scope.clicked = function(postId) {
      if($scope.commentClicked[postId] == null) {
        $scope.commentClicked[postId] = true;
      }
      else {
        $scope.commentClicked[postId] = !($scope.commentClicked[postId]);
      }
      
    } 

    $scope.submitComment = function(postId) {
      var comment = {
        text: $scope.replyText,
        timestamp: new Date().timestamp(),
        author: $scope.userName
      };

      if($scope.comments[postId] == undefined) {
        $scope.comments[postId] = [];
      }
      $scope.comments[postId].push(comment);

      var fd = new FormData();
      var url = '/api/comments/post/' + postId;

      fd.append('text', $scope.replyText);
      fd.append('author', $scope.userName);
      fd.append('classID', $scope.curr);

      $http.post(url, fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined}
      });

      $scope.replyClick[postId] = false;
    }

    $scope.getComments = function(postId) {
      return $scope.comments[postId];
    }

    $scope.reply = function(postId) {
      if($scope.commentClicked[postId] != true) {
        $scope.clicked(postId);
      }
      
      if($scope.replyClick[postId] == null) {
        $scope.replyClick[postId] = true;
      }
      else {
        $scope.replyClick[postId] = !($scope.replyClick[postId]);
      }

    }

     $scope.submitPost = function(currentId) {
      var post = {
        timestamp: new Date().timestamp(),
        title: $scope.postTitle,
        text: $scope.postText,
        priority: $scope.selectedType,
        author: $scope.userName,
        ClassId: currentId
      }

      $scope.posts.push(post);

      
      var fd = new FormData();
      var url = '/api/posts/' + currentId;

      fd.append('text', $scope.postText);
      fd.append('title', $scope.postTitle);
      fd.append('author', $scope.userName);
      fd.append('ClassId', currentId);

      $http.post(url, fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined}
      });

      $scope.newPost = false;
    }

    $scope.prettifyDateTime = function(dateString) {
      if (!dateString) {return;}

      dateString = dateString.replace(/\.\d\d\d+/,""); // remove the milliseconds
      dateString = dateString.replace(/-/,"/").replace(/-/,"/"); //substitute - with /
      dateString = dateString.replace(/T/," ").replace(/Z/," UTC"); //remove T and substitute Z with UTC
      dateString = dateString.replace(/([\+\-]\d\d)\:?(\d\d)/," $1$2"); // +08:00 -> +0800
      var parsed_date = new Date(dateString);
      var relative_to = (arguments.length > 1) ? arguments[1] : new Date(); //defines relative to what ..default is now
      var delta = parseInt((relative_to.getTime()-parsed_date)/1000);
      delta=(delta<2)?2:delta;
      var r = '';
      if (delta < 60) {
        r = delta + ' seconds ago';
      } else if(delta < 120) {
        r = 'a minute ago';
      } else if(delta < (45*60)) {
        r = (parseInt(delta / 60, 10)).toString() + ' minutes ago';
      } else if(delta < (2*60*60)) {
        r = 'an hour ago';
      } else if(delta < (24*60*60)) {
        r = '' + (parseInt(delta / 3600, 10)).toString() + ' hours ago';
      } else if(delta < (48*60*60)) {
        r = 'a day ago';
      } else {
        r = (parseInt(delta / 86400, 10)).toString() + ' days ago';
      }

      return 'about ' + r;
    };


    $scope.newPost = false;



  })
  .directive('studentDiscussionTool', function($http) {
    return {
      controller:'controllerStudentDiscussionTool',
      templateUrl:'/directive-templates/tools-student/template-student-discussion-tool.html',
      scope:{
        curr:'@',
        allClasses:'@all',
        userName:'@user'
      },
    };
  })
;
