angular.module('appDashboard')
  .controller('controllerStudentAssignmentsTool', function($scope, $http, $modal, $log){
  	
  	$scope.assignments = [];
    $scope.allClasses = JSON.parse($scope.allClasses);
    $scope.uploadedFile = {};

    
    for(curr in $scope.allClasses) {
      $scope.url = "/api/assignments/class/" + $scope.allClasses[curr].classId;
      $http.get($scope.url)
        .then(function(res) {
          if(res.data != "") {
            $scope.assignments.push(res.data);
          }
      });
    }
      

    $scope.getAssigns = function(currentId) {
      for(assigns in $scope.assignments) {
        if($scope.assignments[assigns][0].ClassId == currentId) {
          return $scope.assignments[assigns];
        }
      }
    }


    $scope.viewAssignment = function(assignment) {
      // download file

      var doc = {
        filePath: assignment.promptFile
      }

      // DETERMINE FILE TYPE
      var docExtension = doc.filePath.split(".")[doc.filePath.split(".").length - 1];
      var codeFileExtensions = [
        'c', 'cpp', 'h', 'hpp', 'java', 'js', 'py'
      ];


      // VIEW PDF
      if(docExtension == 'pdf') {
        var modalInstance = $modal.open({
          windowClass: 'view-document',
          templateUrl: '/directive-templates/document-views/template-view-document-pdf.html',

          controller: function ($scope, $modalInstance) {
            // INITIALIZE FIELDS WITH DATA
            $scope.assignment = assignment;
            $scope.doc = doc;

            $scope.keyPressed = function(keyCode) {
              if(keyCode == 13) {
                $scope.ok();
              }
            };
            $scope.ok = function () {
              $modalInstance.close($scope.user); // UPDATE VALUE
            };
            $scope.cancel = function () {
              $modalInstance.dismiss('cancel'); // CLOSE WITHOUT UPDATING
            };
          }
        });
      // VIEW CODE
      } else if(codeFileExtensions.indexOf(docExtension) != -1) {
        var modalInstance = $modal.open({
          windowClass: 'view-document',
          templateUrl: '/directive-templates/document-views/template-view-document-code.html',
          controller: function ($scope, $modalInstance) {
            // INITIALIZE FIELDS WITH DATA
            $scope.assignment = assignment;
            $scope.doc = doc;

            $scope.keyPressed = function(keyCode) {
              if(keyCode == 13) {
                $scope.ok();
              }
            };
            $scope.ok = function () {
              $modalInstance.close($scope.user); // UPDATE VALUE
            };
            $scope.cancel = function () {
              $modalInstance.dismiss('cancel'); // CLOSE WITHOUT UPDATING
            };
          }
        });
        modalInstance.opened.then(function () {
          hljs.initHighlightingOnLoad();
        }, function () {
        });

      }
    };

    $scope.viewCodeResult = function(click, programID, assignmentID) {
      var fd = new FormData();
      var url = '/api/assignments/attempt/' + assignmentID;
      var exID;
      var name = $scope.userName;

      fd.append('uploadedFile', $scope.uploadedFile[assignmentID]);
      fd.append('programID', programID);
      fd.append('mainFile', null);

      $http.post(url, fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined}
      }).success(function(res) {
        console.log(res);
        exID = res;
      }).then(function() {
        var modalInstance = $modal.open({
          windowClass: 'view-document',
          templateUrl: '/directive-templates/document-views/template-view-code-result.html',

          controller: function ($scope, $modalInstance, $http) {
            // INITIALIZE FIELDS WITH DATA
            console.log(exID);
            $scope.name = name;
            $scope.name = $scope.name.replace(/ /g,"_");
            $scope.name = $scope.name.toLowerCase();
            $scope.click = click;
            $scope.exID = exID;
            $scope.loading = true;
            var ID = setInterval(function(){
              if($scope.click == true) {
                $http.get('/api/execution/' + $scope.exID)
                 .then(function(res) {
                  console.log(res.data);
                  if(res.data.status === '1') {
                    $scope.execution = res.data;
                    $scope.click = false;
                    $scope.loading = false;
                    $scope.location =  '/results/' + $scope.name + '/results/' + $scope.exID + '-res.txt';
                    clearInterval(ID);
                  }
                 });
               }
             },2000);

            $scope.keyPressed = function(keyCode) {
              if(keyCode == 13) {
                $scope.ok();
              }
            };
            $scope.ok = function () {
              $modalInstance.close($scope.user); // UPDATE VALUE
            };
            $scope.cancel = function () {
              $modalInstance.dismiss('cancel'); // CLOSE WITHOUT UPDATING
            };
          }
        });
      });

      
    }






    $scope.submitAssignment = function(assignmentId) {
      var fd = new FormData();
      var url = '/api/assignments/attempt/' + assignmentId;
      

      fd.append('uploadedFile', $scope.uploadedFile[assignmentId]);
      fd.append('programID', 0);

      $http.post(url, fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined}
      });

    };




  })
  .directive('studentAssignmentsTool', function() {
    return {
      controller:'controllerStudentAssignmentsTool',
      templateUrl:'/directive-templates/tools-student/template-student-assignments-tool.html',
      scope: {
        curr:'@curr',
        allClasses:'@all',
        userName:'@user'
      },
      link: function( scope, elem, attrs, controller ) {

      }
    };
  }).directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
  }])
;
