angular.module('appDashboard')
  .controller('controllerStudentGradesTool', function($scope, $http){
 
  	$scope.assignments = [];
  	$scope.grades = [];
    $scope.allClasses = JSON.parse($scope.allClasses);
    $scope.set = true;

    
    //have to iterate through this
    for(curr in $scope.allClasses) {
      $scope.url = /grades/ + $scope.allClasses[curr].classId;
      $http.get($scope.url)
        .then(function(res) {
          if(res.data != "") {
            $scope.grades.push(res.data);
          }
      });
    }
    

    $scope.currGrades = function(currentId) {
      if($scope.set) {
        var gradeNums = [];
      	var assignNames = [];
      	var gradeBool = false;
      	

      	for(currGrade in $scope.grades) {
      		if($scope.grades[currGrade][0].classID == currentId) {
      			gradeBool = true;
            for(classGrade in $scope.grades[currGrade])
      			   gradeNums.push($scope.grades[currGrade][classGrade]);
      		}
      	}

        return gradeNums;
      }
    }

   })
  .directive('studentGradesTool', function() {
    return {
      controller:'controllerStudentGradesTool',
      templateUrl:'/directive-templates/tools-student/template-student-grades-tool.html',
      scope:{
        curr:'@',
        allClasses:'@all'
      }
    };
  })
;
