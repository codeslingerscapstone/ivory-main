angular.module('appDashboard')
  .controller('controllerStudentAttendanceTool', function($scope, $http){

    $scope.allClasses = JSON.parse($scope.allClasses);  //JSONifying the string
    $scope.attendances = [$scope.allClasses.length];    //creating array of attendances
    $scope.index = 0;                                   //index for the array
    $scope.set = false;                                 //whether the data has been grabbed

    //For all classes the student is in
    for(section in $scope.allClasses) {
      //grab the attendance data for the class
      $scope.url = '/users/attendance/' + $scope.allClasses[section].classId;
      $http.get($scope.url)
      .then(function(res) {
        //store the data in the array
        if(res.data.length > 0) {
          $scope.attendances[$scope.index] = res.data;
          $scope.index++;
          $scope.set = true;
        }
      }); 
    }

    //get attendace for a specific class
    $scope.getAttendance = function(currentId) {
      if($scope.set == true) {
        //If the attendance is for the correct class, display it
        for(var attend in $scope.attendances) {
          if($scope.attendances[attend][0].classId == currentId) {
            return $scope.attendances[attend];
          }
        }
      }
    };

    $scope.getNumAttendances = function(currentId) {
      if(typeof $scope.getAttendance(currentId) != "undefined") {
        return $scope.getAttendance(currentId).length;
      } else {
        return 0;
      }
    };
    $scope.getNumAbsences = function(currentId) {
      var numAbsences = 0;
      var attendance = $scope.getAttendance(currentId);
      if(typeof attendance != "undefined") {
        for(var a in attendance) {
          if(attendance[a].status == 'UA' || attendance[a].status == 'EA') {
            numAbsences++;
          }
        }
      }
      return numAbsences;
    };
    $scope.getNumLates = function(currentId) {
      var numLates = 0;
      var attendance = $scope.getAttendance(currentId);
      if(typeof attendance != "undefined") {
        for(var a in attendance) {
          if(attendance[a].status == 'UL' || attendance[a].status == 'EL') {
            numLates++;
          }
        }
      }
      return numLates;
    };
    $scope.getNumPresents = function(currentId) {
      var numPresents = 0;
      var attendance = $scope.getAttendance(currentId);
      if(typeof attendance != "undefined") {
        for(var a in attendance) {
          if(attendance[a].status == 'P') {
            numPresents++;
          }
        }
      }
      return numPresents;
    };

  })
  .directive('studentAttendanceTool', function($http) {
    return {
      controller:'controllerStudentAttendanceTool',
      templateUrl:'/directive-templates/tools-student/template-student-attendance-tool.html',
      scope:{
        curr:'@',
        allClasses:'@all'
      },
    };
  })
   .filter('attendstatus', function() {
    return function(input) {
        switch(input) {
          case 'P':
            return 'Present';
            break;
          case 'UA':
            return 'Absent';
            break;
          case 'UL':
            return 'Late';
            break;
          case 'EA':
            return 'Absent (excused)';
            break;
          case 'EL':
            return 'Late (excused)';
            break;
        }
      }
  })
;
