angular.module('appDashboard')
  .controller('controllerStudentSyllabusTool', function($scope, $http){

      $scope.allClasses = JSON.parse($scope.allClasses);
      $scope.syllabi = [];
      $scope.index = 0;

   
      for(curr in $scope.allClasses) {
        $scope.url = /syllabus/ + $scope.allClasses[curr].classId;
    
        $http.get($scope.url)
          .then(function(res) {
            if(res.data != "") {
              $scope.syllabi.push(res.data);
              $scope.index++;
            }
        });
      }

      $scope.showSyllabus = function(currentId) {
        for(syllabus in $scope.syllabi) {
          if($scope.syllabi[syllabus].classID == currentId) {
            return $scope.syllabi[syllabus];
          }
        } 
      }
  })
  .directive('studentSyllabusTool', function() {
    return {
      controller:'controllerStudentSyllabusTool',
      templateUrl:'/directive-templates/tools-student/template-student-syllabus-tool.html',
      scope:{
        curr:'@',
        allClasses:'@all'
      }
    };
  })
;
