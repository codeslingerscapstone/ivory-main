var appSignup = angular.module('appSignup', ['ngAnimate', 'ui.bootstrap', 'moduleGlobal']);

appSignup
  .config([function(){
    // any configuration goes here
  }])
  .controller('controllerSignup', function($scope) {
  	$scope.pageName = "signup"
    // set up scope here
  });
