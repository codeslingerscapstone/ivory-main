
var appDashboard = angular.module('appDashboard', ['ngAnimate', 'ui.bootstrap', 'moduleGlobal', 'ng-context-menu', 'hljs']);

appDashboard
  .config([function(){
    // any configuration goes here
  }])
  .controller('controllerDashboard', function($scope, $http, $modal, $log) {
    $scope.pageName = "dashboard";
    
    $http.get('/users/me')
      .then(function(res) {
        $scope.userName = res.data['name'];
      });
    $scope.prefs = [];
    $scope.enabled = {};
    $scope.menu = false;

    $scope.getCurrentClass = function() {
      return $scope.currentClass;
    };


    $scope.setCurrentClass = function (currClass, currRole) {
      $scope.currentClass = currClass;
      $scope.role = currRole;
    };

    $scope.getCurrentClassId = function() {
      for(var i=0; i< $scope.allClasses.length; i++) {
        if($scope.allClasses[i].courseNum == $scope.currentClass){
          return $scope.allClasses[i].classId;
        }
      }
    };

    $scope.clicked = function(id) {
      if($scope.enabled[id] == undefined) {
        $scope.enabled[id] = false;
      }
      else {
        $scope.enabled[id] = !($scope.enabled[id]);
      }
    }

    $scope.menuClick = function() {
      $scope.menu = !($scope.menu);
    }

    $http.get('/users/classes.json')
      .then(function(res) {
      $scope.allClasses = res.data;

    });

    //Modal methods
    $scope.open = function(classId) {
      $scope.prefs = [];
      $http.get('/notifications/class/' + classId)
       .then(function(res) {
          $scope.prefs = res.data;
       }).then(function() {
        var modalInstance = $modal.open({
          templateUrl: 'settingsTab.html',
          controller: ModalInstanceCtrl,
          resolve: {
            prefs: function() {
              return $scope.prefs;
            },
            role: function() {
              return $scope.role;
            },
            allClasses: function() {
              return $scope.allClasses;
            },
            classId: function() {
              return classId;
            }
          }
        });

        modalInstance.result.then(function (selectedItem) {
          $scope.selected = selectedItem;
        }, function () {
          
        });
      });
    };

    // Please note that $modalInstance represents a modal window (instance) dependency.
    // It is not the same as the $modal service used above.

    var ModalInstanceCtrl = function ($scope, $modalInstance, $http, prefs, role, allClasses, classId) {

      $scope.types = [
        'Grades',
        'Attendance',
        'Assignments',
        'Discussion'
      ];

      $scope.tabs = [
        'Syllabus',
        'Assignments',
        'Grades',
        'Attendance',
        'Discussion'
      ];

      $scope.newPrefs = {};
      $scope.enabled = {};


      $scope.role = role;
      $scope.allClasses = allClasses;

      for(type in $scope.types) {
        if(prefs.indexOf($scope.types[type]) != -1) {
          $scope.newPrefs[$scope.types[type]] = true;
        }
        else {
          $scope.newPrefs[$scope.types[type]] = false;
        }
      }

      $scope.prefTypeId = function(type) {
        switch(type) {
          case "Grades":
            return 1;
            break;
          case "Attendance":
            return 2;
            break;
          case "Assignments":
            return 3;
            break;
          case "Discussion":
            return 4;
            break;
        }
      };

      
      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };

      $scope.save = function() {
        if(role == 1) {
          for(newPref in $scope.newPrefs) {
            if($scope.newPrefs[newPref] == true) {
              if(prefs.indexOf(newPref) == -1) {
                var url = '/notifications/class/' + classId;
                var fd = new FormData();

                fd.append('PrefID', $scope.prefTypeId(newPref));

                $http.post(url, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                });   
              }
            }
          }
          for(oldPref in prefs) {
            if($scope.newPrefs[prefs[oldPref]] == false) {
              var url = '/notifications/class/' + classId + '/prefType/' + $scope.prefTypeId(prefs[oldPref]);
           
              $http.delete(url);
            }
          }
        }
        else if(role == 2) {

        }
        $scope.cancel();
      }
    };

    $scope.displayDocument = function() {
      // download document to local directory
      // 
    };
  
  })
;
