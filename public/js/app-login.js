var appLogin = angular.module('appLogin', ['ngAnimate', 'ui.bootstrap', 'moduleGlobal']);

appLogin
  .config([function(){
    // any configuration goes here
  }])
  .controller('controllerLogin', function($scope) {
  	$scope.pageName = "login";
    // set up scope here
  });
