angular.module('appDashboard')

  // Controller
  .controller('controllerMyDirective', function($scope){
    // PUT DATA AND LOGIC HERE
    $scope.stuff = {
      thing: "blah blah",
      thing2: {
        foonum: 213,
        words: "hi hi hi"
      }
    };

    $scope.doStuff = function() {
      $scope.thing2.foonum += 1;
      return $scope.thing + $scope.thing2.words;
    };
  })

  // Directive
  .directive('myDirective', function() {
    return {
      controller:'controllerMyDirective',                             // THE CONTROLLER DEFINED ABOVE
      templateUrl:'directive-templates/template-my-directive.html',   // THE TEMPLATE FILE
      scope: {                                                        // BINDING HTML ATTRIBUTES TO SCOPE VARIABLES
        tool:'=tool'
      }
    };
  })
;
