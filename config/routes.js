// Middleware
var auth  = require('./middlewares/authorization.js');

// Controllers
var assignments = require('../app/controllers/assignments');
var attendance = require('../app/controllers/attendance');
var classes = require('../app/controllers/classes');
var comments = require('../app/controllers/comments');
var documents = require('../app/controllers/documents');
var grades = require('../app/controllers/grades');
var index = require('../app/controllers/index');
var notifications = require('../app/controllers/notifications');
var posts = require('../app/controllers/posts');
var syllabus = require('../app/controllers/syllabus');
var users = require('../app/controllers/users');

// Initialize all the routes
exports.init = function(app, passport, auth) {
    console.log('Initializing Routes');

    // Home routes (these are the only ones that actually render pages)
    app.get('/', index.index);
    app.get('/app', index.app);
    app.get('/login', index.login);
    app.get('/signup', index.signup);
    app.get('/admin', index.admin);

    // Authentication routes
    app.post('/users', users.create);
    app.post('/users/login', passport.authenticate('local', {
        failureRedirect: '/login',
        failureFlash: true
    }), users.login);
    app.get('/users/logout', users.logout);
    app.get('/users/me', users.me);
    app.get('/users/classes.json', users.userClasses);

    // Finish with setting up the userId param
    app.param('userId', users.user);
    app.param('classId', classes.classObj);
    app.get('/users/attendance/:classId', attendance.stuAttend);

    // API
    app.namespace('/api', function () {
        // Attendance resource
        // done
        process.stdout.write("API: Attendance resource                    \r");
        app.namespace('/attendance', function () {
            app.namespace('/class/:classID', function () {
                // TODO: make return name and id
                app.get('/', attendance.getClassAttendance);
                // TODO: make return name and id
                app.get('/user/:userID', attendance.getStudentAttendance);
                app.post('/user/:userID', attendance.addOrUpdateUserAttendance);
            });
        });

        // Assignments resource
        // done
        process.stdout.write("API: Assignments resource                   \r");
        app.namespace('/assignments', function () {
            app.get('/:assignmentID', assignments.getAssignment);
            app.put('/:assignmentID', assignments.updateAssignment);
            app.delete('/:assignmentID', assignments.deleteAssignment);
            app.get('/class/:classID', assignments.getClassAssignments);
            app.post('/class/:classID', assignments.addClassAssignment);
            app.post('/attempt/:assignmentID', assignments.submitAttempt);
        });

        // Classes resource
        process.stdout.write("API: Classes resource                       \r");
        app.namespace('/classes', function () {
            app.get('/:classID', classes.getClass);
            app.get('/user/:userID', classes.getClassesForUserWithRole);
            app.post('/', classes.addClass);
            app.put('/:classID', classes.updateClass);
            app.delete('/:classID', classes.deleteClass);
            app.post('/:classID/user/:userID', classes.addOrUpdateClassUserRole);
            app.get('/prof/allUsers', classes.allUsersForProf);
        });

        // Comments resource
        process.stdout.write("API: Comments resource                      \r");
        app.namespace('/comments', function () {
            app.post('/post/:postID',comments.newComment);
            app.delete('/:commentID', comments.deleteComment);
        });

        // Documents resource
        process.stdout.write("API: Documents resource                     \r");
        app.namespace('/documents', function () {
            // TODO: implement
        });

        // Grades resource
        process.stdout.write("API: grades resource                        \r");
        app.namespace('/grades', function () {
            // TODO: implement
        });

        // Notifications resource
        process.stdout.write("API: Notifications resource                 \r");
        app.namespace('/notifications', function () {
            // TODO: implement
        });

        // Posts resource
        process.stdout.write("API: Posts resource                         \r");
        app.namespace('/posts', function () {
            app.get('/:classID', posts.getPosts);
            app.post('/:classID', posts.newPost);
            app.delete('/:postID', posts.deletePost);
        });

        // Syllabus resource
        // done
        process.stdout.write("API: Syllabus resource                      \r");
        app.namespace('/syllabus', function () {
            app.get('/class/:classID', syllabus.getSyllabus);
            app.post('/class/:classID', syllabus.uploadSyllabus);
        });

        // Users resource
        // done
        process.stdout.write("API: Users resource                         \r");
        app.namespace('/users', function () {
            app.get('/me', users.me);
            app.get('/:userID', users.getUser);
            app.post('/', users.addUser);
            app.put('/', users.updateUser);
            app.delete(':userID', users.deleteUser);
        });

        process.stdout.write("API: Execution resource                     \r");
        app.namespace('/execution', function () {
            app.get('/:exID', assignments.getExecution);
        });
    });

    //TEMPORARY, PRETTY AWFUL
    app.get('/users/attendance/:classId', attendance.stuAttend); // moved to 
    //app.get('/api/assignments/:classID', assignments.getClassAssignments); // moved to /api/assignments/class/:classId
    app.get('/users/attendance/:classId/:userId', attendance.teachAttend); // attendance
    // app.get('/classes/allUsers', classes.allUsers); // moved to /api/classes/prof/allUsers
    app.post('/users/attendance/:classId/:userId/:status/:date', attendance.postAttend); // attendance
    app.put('/users/attendance/:classId/:userId/:status/:date', attendance.updateAttend); // attendance
    app.get('/grades/:classId', grades.getGrades); // grades
    app.get('/grades/:classId/:userId', grades.getTeacherGrades); // grades
    app.put('/grades/:classId/:userId/:assignId/:grade', grades.updateGrade); //grades
    app.get('/syllabus/:classId', syllabus.getSyllabus); // syllabus
    app.get('/documents/:documentId', documents.getDoc); // documents
    //app.get('/posts/:classId', posts.getPosts); // moved to /api/posts/:classID
    app.get('/posts/comments/:postId', comments.getComments); // posts
    app.get('/notifications/class/:classID', notifications.getNotificationPrefs); // notifications
    app.post('/notifications/class/:classID', notifications.setNotificationPrefs);
    app.delete('/notifications/class/:classID/prefType/:prefID', notifications.deleteNotificationPrefs);
    app.get('/notifications/', notifications.getNotifications); // notifications
    app.delete('/notifications/class/:classId/type/:typeId', notifications.clearNotifications); // notifications
    app.get('/admin/users', users.allUsers);
    app.get('/admin/classes', classes.allClasses);
    app.get('/admin/courses', classes.allCourses);
};
