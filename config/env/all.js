var path = require('path'),
rootPath = path.normalize(__dirname + '/../..');

module.exports = {
	root: rootPath,
    ivoryPath: process.env.HOME + '/ivory',
	port: process.env.PORT || 7000,
	modelsDir : rootPath + '/app/models'
    // db: process.env.MONGOHQ_URL    
}
