exports.init = function(db) {
	var obj = db.Course.build({
		id: 1,
		department: 'CSI',
		number: '1234',
		name: 'Intro to Computer Science I'
	});
	obj.save().error(function(err) {
		console.log(err);
	});
	
	obj = db.Course.build({
		id: 2,
		department: 'CSI',
		number: '2334',
		name: 'Discrete Structures'
	});
	obj.save().error(function(err) {
		console.log(err);
	});
	
	obj = db.Class.build({
		id: 1,
		closed: false,
		time: '3:00',
		location: 'Rogers 110',
		numberofSeats: 25,
		CourseId: 1
	});
	obj.save().error(function(err) {
		console.log(err);
	});
	
	obj = db.Class.build({
		id: 2,
		closed: false,
		time: '2:00',
		location: 'Rogers 110',
		numberofSeats: 25,
		CourseId: 2
	});
	obj.save().error(function(err) {
		console.log(err);
	});
	
	obj = db.Role.build({
		id: 1,
		name: 'Student'
	});
	obj.save().error(function(err) {
		console.log(err);
	});
	
	obj = db.Role.build({
		id: 2,
		name: 'Instructor'
	});
	obj.save().error(function(err) {
		console.log(err);
	});
	
	obj = db.User.build({
		id: 1,
		name: 'Andrew Wilson',
		username: 'aStudent',
		hashedPassword: 'u5Lb4Z0isf+MVt/+Lig/wrJJ40tnbwyZJGGkZJs2AZBLBTu7pc+GuDFZpbxjqo+0x264PoiUK5jxIvgfgt31Cg==',
		salt: 'Dex945d08cEfa3tB3ix1Ng==',
		email: 'email@mail.com'
	});
	obj.save().error(function(err) {
		console.log(err);
	});
	
	
	obj = db.User.build({
		id: 2,
		name: 'Jeff Donahoo',
		username: 'aProf',
		hashedPassword: 'soqenD9jqXy9+NMmMeKRoLgmowLj05Ecp1Ozdt6YuXG9Oz+wfoNgIfHQOIZo0GmC2ZzXv5T019g10k8iVUhOGQ==',
		salt: 'f4KMeqsS9mhRttCkcfJKgA==',
		email: 'profmail@mail.com'
	});
	obj.save().error(function(err) {
		console.log(err);
	});
	
	obj = db.User.build({
		id: 3,
		name: 'Austin Bratcher',
		username: 'aStudent2',
		hashedPassword: 'oVdgIekvMdV68WWfmzZl9vEyu+wQGMwbVLu9jHtYO/nof312x+RXa9OKUGi3BgICMQccmp7HiZU7Fa89p4erMw==',
		salt: '47PvbsOtST8clqzYJ7Eigw==',
		email: 'email2@mail.com'
	});
	obj.save().error(function(err) {
		console.log(err);
	});
	
	obj = db.User.build({
		id: 4,
		name: 'Chris Gephart',
		username: 'aStudent3',
		hashedPassword: '8L6CwYs06qb/o6d/FaHNvpMjkn+AucvRJcxgC3WA2o0W5QnzrHa0OAm8X0LzkI0hCo4Vx7Kk68cxg0trDzI0oA==',
		salt: 'VKjwgNIzjoPzyl+j3/b0iA==',
		email: 'email3@mail.com'
	});
	obj.save().error(function(err) {
		console.log(err);
	});
	
	obj = db.ClassUser.build({
		id: 1,
		RoleId: 1,
		ClassId: 1,
		UserId: 1
	});
	obj.save().error(function(err) {
		console.log(err);
	});
	
	obj = db.ClassUser.build({
		id: 2,
		RoleId: 1,
		ClassId: 2,
		UserId: 1
	});
	obj.save().error(function(err) {
		console.log(err);
	});
	
	obj = db.ClassUser.build({
		id: 3,
		RoleId: 2,
		ClassId: 1,
		UserId: 2
	});
	obj.save().error(function(err) {
		console.log(err);
	});
	
	obj = db.ClassUser.build({
		id: 4,
		RoleId: 2,
		ClassId: 2,
		UserId: 2
	});
	obj.save().error(function(err) {
		console.log(err);
	});
	
	obj = db.ClassUser.build({
		id: 5,
		RoleId: 1,
		ClassId: 1,
		UserId: 3
	});
	obj.save().error(function(err) {
		console.log(err);
	});

	
	obj = db.ClassUser.build({
		id: 6,
		RoleId: 1,
		ClassId: 2,
		UserId: 4
	});
	obj.save().error(function(err) {
		console.log(err);
	});
	
	obj = db.Attendance.build({
		id: 1,
		date: '2014-02-24',
		status: 'P',
		UserId: 1,
		ClassId: 1
	});
	obj.save().error(function(err) {
		console.log(err);
	});
	
	obj = db.Attendance.build({
		id: 2,
		date: '2014-02-25',
		status: 'UA',
		UserId: 1,
		ClassId: 1
	});
	obj.save().error(function(err) {
		console.log(err);
	});
	
	obj = db.Attendance.build({
		id: 3,
		date: '2014-02-26',
		status: 'UL',
		UserId: 1,
		ClassId: 1
	});
	obj.save().error(function(err) {
		console.log(err);
	});
	
	obj = db.Attendance.build({
		id: 4,
		date: '2014-02-24',
		status: 'EL',
		UserId: 1,
		ClassId: 2
	});
	obj.save().error(function(err) {
		console.log(err);
	});
	
	obj = db.Attendance.build({
		id: 5,
		date: '2014-02-24',
		status: 'P',
		UserId: 3,
		ClassId: 1
	});
	obj.save().error(function(err) {
		console.log(err);
	});
	
	obj = db.Attendance.build({
		id: 6,
		date: '2014-02-24',
		status: 'P',
		UserId: 4,
		ClassId: 2
	});
	obj.save().error(function(err) {
		console.log(err);
	});
	
};