var db = require('./config/sequelize');
var schedule = require('node-schedule');
var nodemailer = require("nodemailer");

module.exports = function () {
	var smtpTransport = nodemailer.createTransport("SMTP",{
	    host: "smtp.gmail.com", // hostname
	    secureConnection: true, // use SSL
	    port: 465, // port for secure SMTP
	    auth: {
	        user: "ivorymailer@gmail.com",
	        pass: "IvoryTheBest"
	    }
	});

	var rule = new schedule.RecurrenceRule();
	rule.minute = 0;

	var job = schedule.scheduleJob(rule, function() {

		var users = {};
		var IDArr = [];
		var notifPrefs = {};
		var index = -1;

		var emailStr = "Notifications from the past hour:\n\n"; 

		db.Notification.findAll().success(function(notifications) {
			for(notif in notifications) {
				if(users[notifications[notif].UserId] == undefined) {
					users[notifications[notif].UserId] = [];
					IDArr.push(notifications[notif].UserId);
				}
				users[notifications[notif].UserId].push(notifications[notif]);
			}

			for(ID in users) {
				db.NotificationPreference.findAll({where: {UserId: ID}}).success(function(prefs) {
					notifPrefs[ID] = prefs;
				}).error(function(err) {
					console.log(err);
				}).then(function() {
					index++;
					db.User.find(IDArr[index]).success(function(user) {
					    for(notif in users[user.id]) {
							if(users[user.id][notif].toEmail == true) {
								for(pref in notifPrefs[IDArr[index]]) {
									if(users[user.id][notif].ClassId == notifPrefs[IDArr[index]][pref].ClassId && 
											users[user.id][notif].NotificationTypeId == notifPrefs[IDArr[index]][pref].NotificationTypeId) {
										emailStr+=users[user.id][notif].text + '\n';
										users[user.id][notif].updateAttributes({toEmail: false});
									}
								}
							}
						}

						if(emailStr !== "Notifications from the past hour:\n\n") {
							var mailOptions = {
						        from: "Ivory <no-reply@ivory.com>", // sender address
						        to: user.email, // list of receivers
						        subject: 'Ivory Notifications', // Subject line
						        text: emailStr // plaintext body
						    }    


						    smtpTransport.sendMail(mailOptions, function(error, response){
						        if(error){
						            console.log(error);
						        }else{
						            console.log("Message sent: " + response.message);
						        }
						    });
						}

					}).error(function(err) {
						console.log(err);
					});
				});
			}
		}).error(function(err) {
			console.log(err);
		});
	});
}