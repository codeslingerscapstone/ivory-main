#!/bin/bash

echo "Inserting values..."
psql sourcedock-dev << EOF

	INSERT INTO "Courses" ("department","number","name") VALUES ('CSI','1234','Intro to Computer Science I');
	INSERT INTO "Classes" ("closed", "time", "location", "numberOfSeats", "CourseId", "days") VALUES (false, '3:00', 'Rogers 110', 25,1,'MWF');

	INSERT INTO "Courses" ("department","number","name") VALUES ('CSI','4321','Networking');
	INSERT INTO "Classes" ("closed", "time", "location", "numberOfSeats", "CourseId", "days")  VALUES (false, '2:00', 'Rogers 110', 25,2,'MWF');

	INSERT INTO "Courses" ("department","number","name") VALUES ('CSI','4C34','Capstone');
	INSERT INTO "Classes" ("closed", "time", "location", "numberOfSeats", "CourseId", "days")  VALUES (false, '1:00', 'Rogers 110', 25,3,'TR');

	INSERT INTO "Courses" ("department","number","name") VALUES ('CSI','4334','Operating Systems');
    INSERT INTO "Classes" ("closed", "time", "location", "numberOfSeats", "CourseId", "days")  VALUES (false, '4:00', 'Rogers 110', 25, 4, 'TR');


	INSERT INTO "Roles" ("name") VALUES ('Student');
	INSERT INTO "Roles" ("name") VALUES ('Instructor');

    INSERT INTO "Users" ("name","username","hashedPassword","salt","email", "occupation") VALUES ('Andrew Wilson','Andrew_Wilson','u5Lb4Z0isf+MVt/+Lig/wrJJ40tnbwyZJGGkZJs2AZBLBTu7pc+GuDFZpbxjqo+0x264PoiUK5jxIvgfgt31Cg==','Dex945d08cEfa3tB3ix1Ng==','Andrew_Wilson1@baylor.edu','Computer Science');
	INSERT INTO "ClassUsers" ("ClassId", "RoleId", "UserId") VALUES (3,1,1);
	INSERT INTO "ClassUsers" ("ClassId", "RoleId", "UserId") VALUES (4,1,1);


	INSERT INTO "Users" ("name","username","hashedPassword","salt","email", "occupation") VALUES ('Jeff Donahoo','Jeff_Donahoo','soqenD9jqXy9+NMmMeKRoLgmowLj05Ecp1Ozdt6YuXG9Oz+wfoNgIfHQOIZo0GmC2ZzXv5T019g10k8iVUhOGQ==','f4KMeqsS9mhRttCkcfJKgA==','profemail@mail.com','Computer Science');
	INSERT INTO "ClassUsers" ("ClassId", "RoleId", "UserId") VALUES (1,2,2);
	INSERT INTO "ClassUsers" ("ClassId", "RoleId", "UserId") VALUES (2,2,2);
	INSERT INTO "ClassUsers" ("ClassId", "RoleId", "UserId") VALUES (3,2,2);
	INSERT INTO "ClassUsers" ("ClassId", "RoleId", "UserId") VALUES (4,2,2);

	INSERT INTO "Users" ("name","username","hashedPassword","salt","email", "occupation") VALUES ('Austin Bratcher','Autsin_Bratcher','oVdgIekvMdV68WWfmzZl9vEyu+wQGMwbVLu9jHtYO/nof312x+RXa9OKUGi3BgICMQccmp7HiZU7Fa89p4erMw==','47PvbsOtST8clqzYJ7Eigw==','email@mail.com','Computer Science');
	INSERT INTO "ClassUsers" ("ClassId", "RoleId", "UserId") VALUES (4,1,3);

	INSERT INTO "Users" ("name","username","hashedPassword","salt","email", "occupation") VALUES ('Kevin Smith','Kevin_Smith','8L6CwYs06qb/o6d/FaHNvpMjkn+AucvRJcxgC3WA2o0W5QnzrHa0OAm8X0LzkI0hCo4Vx7Kk68cxg0trDzI0oA==','VKjwgNIzjoPzyl+j3/b0iA==','email@mail.com','Computer Science');
	INSERT INTO "ClassUsers" ("ClassId", "RoleId", "UserId") VALUES (2,1,4);
	INSERT INTO "ClassUsers" ("ClassId", "RoleId", "UserId") VALUES (3,1,4);
	INSERT INTO "ClassUsers" ("ClassId", "RoleId", "UserId") VALUES (4,1,4);

	INSERT INTO "Users" ("name","username","hashedPassword","salt","email", "occupation") VALUES ('Brandon Smith','Brandon_Smith','8L6CwYs06qb/o6d/FaHNvpMjkn+AucvRJcxgC3WA2o0W5QnzrHa0OAm8X0LzkI0hCo4Vx7Kk68cxg0trDzI0oA==','VKjwgNIzjoPzyl+j3/b0iA==','email@mail.com','Computer Science');
	INSERT INTO "ClassUsers" ("ClassId", "RoleId", "UserId") VALUES (2,1,5);
	INSERT INTO "ClassUsers" ("ClassId", "RoleId", "UserId") VALUES (3,1,5);

	INSERT INTO "Users" ("name","username","hashedPassword","salt","email", "occupation") VALUES ('Matt Seale','Matt_Seale','8L6CwYs06qb/o6d/FaHNvpMjkn+AucvRJcxgC3WA2o0W5QnzrHa0OAm8X0LzkI0hCo4Vx7Kk68cxg0trDzI0oA==','VKjwgNIzjoPzyl+j3/b0iA==','email@mail.com','Statistics');
	INSERT INTO "ClassUsers" ("ClassId", "RoleId", "UserId") VALUES (1,1,6);

	INSERT INTO "Users" ("name","username","hashedPassword","salt","email", "occupation") VALUES ('Chris Gephart','Chris_Gephart','8L6CwYs06qb/o6d/FaHNvpMjkn+AucvRJcxgC3WA2o0W5QnzrHa0OAm8X0LzkI0hCo4Vx7Kk68cxg0trDzI0oA==','VKjwgNIzjoPzyl+j3/b0iA==','email@mail.com','Computer Science');
	INSERT INTO "ClassUsers" ("ClassId", "RoleId", "UserId") VALUES (3,1,7);
	INSERT INTO "ClassUsers" ("ClassId", "RoleId", "UserId") VALUES (4,1,7);

	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-02-24', 'P',1,6);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-02-26', 'P',1,6);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-02-28', 'P',1,6);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-03-03', 'UA',1,6);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-03-05', 'UA',1,6);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-03-07', 'UL',1,6);

	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-02-24', 'P',2,4);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-02-26', 'P',2,4);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-02-28', 'P',2,4);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-03-03', 'P',2,4);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-03-05', 'P',2,4);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-03-07', 'P',2,4);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-02-24', 'UA',2,5);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-02-26', 'P',2,5);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-02-28', 'P',2,5);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-03-03', 'P',2,5);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-03-05', 'EA',2,5);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-03-07', 'P',2,5);

	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-02-24', 'P',4,1);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-02-26', 'P',4,1);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-02-28', 'P',4,1);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-03-03', 'P',4,1);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-03-05', 'P',4,1);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-03-07', 'P',4,1);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-02-24', 'P',4,3);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-02-26', 'P',4,3);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-02-28', 'P',4,3);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-03-03', 'P',4,3);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-03-05', 'P',4,3);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-03-07', 'P',4,3);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-02-24', 'P',4,4);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-02-26', 'P',4,4);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-02-28', 'UA',4,4);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-03-03', 'UA',4,4);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-03-05', 'UA',4,4);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-03-07', 'UA',4,4);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-02-24', 'P',4,7);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-02-26', 'P',4,7);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-02-28', 'P',4,7);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-03-03', 'EA',4,7);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-03-05', 'P',4,7);
	INSERT INTO "Attendances" ("date", "status", "ClassId", "UserId") VALUES ('2014-03-07', 'P',4,7);

	INSERT INTO "Documents" ("created","text") VALUES ('2014-02-24','Learn how to computer.');
	INSERT INTO "Documents" ("created","text") VALUES ('2014-02-24','Make a website from scratch, for free');
	INSERT INTO "Documents" ("created","text") VALUES ('2014-02-24','A Successful Student...

	-Uses all available resources
	-Takes notes
	-Can apply knowledge - Memorization is only the start!
	-Keeps up
	-No test/quiz cramming
	-Reading
	-Review notes
	-Starts early on all assignments
	-Seek help in plenty of time
	-Assignments take less time
	
	Information Sources

	-Reading
	-Lecture
	-Class Web Page
	-Ivory
	-Instructor
	-Homework Assignments
	-Program Specifications');
	
	INSERT INTO "Documents" ("created","text") VALUES ('2014-02-24','Testing testing, can anyone hear me?');
	INSERT INTO "Documents" ("created","text") VALUES ('2014-02-24','Read Pages 1-500 in the textbook');
	INSERT INTO "Documents" ("created","text") VALUES ('2014-02-24','Exam covering material from all the powerpoints, chapters 1-5, and lectures.');
	INSERT INTO "Documents" ("created","text") VALUES ('2014-02-24','Write an essay about how amazing Networking is. It will need to be at least 10 pages long, covering such topics as how much you love networking, why you like spending 50 horus on a program, and how much you love race conditions.');
	INSERT INTO "Documents" ("created","text") VALUES ('2014-02-24','Determine if P=NP. Write a program to demonstrate your answer');
	INSERT INTO "Documents" ("created","text") VALUES ('2014-02-24','Submit a picture of a computer. Draw it with at least 3 colors.');
	INSERT INTO "Documents" ("created","text") VALUES ('2014-02-24','Final Exam covering all topics learned in this class.');
	INSERT INTO "Documents" ("created","text") VALUES ('2014-02-24','Submit your answers to problems in the book: 9.1, 9.4, 9.10, 9.15, 9.20, and 9.55');
	INSERT INTO "Documents" ("created","text") VALUES ('2014-02-24','Write the most sophisticated AI known to man.');
	INSERT INTO "Documents" ("created","text") VALUES ('2014-02-24','Capstone Presentation for Pariveda.');
	INSERT INTO "Documents" ("created","text") VALUES ('2014-02-24','Testing testing');
	INSERT INTO "Documents" ("created","text") VALUES ('2014-02-24','Computers are good!');
	

	INSERT INTO "Syllabuses" ("ClassId", "DocumentId") VALUES (1,1);
	INSERT INTO "Syllabuses" ("ClassId", "DocumentId") VALUES (3,2);
	INSERT INTO "Syllabuses" ("ClassId", "DocumentId") VALUES (4,3);


	INSERT INTO "Assignments" ("title","created","due","tokenNum","tokenType","submission","ClassId") VALUES ('Test Assignment','2014-03-03', '2014-03-14',-1,'total',false,1);
	INSERT INTO "AssignmentPrompts" ("DocumentId", "AssignmentId") VALUES (4,1);
	INSERT INTO "AssignmentAttempts" ("timestamp", "grade", "AssignmentId", "UserId", "DocumentId") VALUES ('2014-03-05',100,1,6,14);

	INSERT INTO "Assignments" ("title","created","due","tokenNum","tokenType","submission","ClassId") VALUES ('Textbook Reading', '2014-03-03', '2014-03-09',-1,'total',false,4);
	INSERT INTO "AssignmentPrompts" ("DocumentId", "AssignmentId") VALUES (5,2);

	INSERT INTO "Assignments" ("title","created","due","tokenNum","tokenType","submission","ClassId") VALUES ('Exam 1','2014-03-04', '2014-03-05',-1,'total',false,4);
	INSERT INTO "AssignmentPrompts" ("DocumentId", "AssignmentId") VALUES (6,3);
	INSERT INTO "AssignmentAttempts" ("timestamp", "grade", "AssignmentId", "UserId") VALUES ('2014-03-05',80,3,1);
	INSERT INTO "AssignmentAttempts" ("timestamp", "grade", "AssignmentId", "UserId") VALUES ('2014-03-05',75,3,3);
	INSERT INTO "AssignmentAttempts" ("timestamp", "grade", "AssignmentId", "UserId") VALUES ('2014-03-05',95,3,4);
	INSERT INTO "AssignmentAttempts" ("timestamp", "grade", "AssignmentId", "UserId") VALUES ('2014-03-05',90,3,7);

	INSERT INTO "Assignments" ("title","created","due","tokenNum","tokenType","submission","ClassId") VALUES ('"Networking is the BEST": The Essay','2014-02-26', '2014-03-14',3,'total',true,2);
	INSERT INTO "AssignmentPrompts" ("DocumentId", "AssignmentId") VALUES (7,4);

	INSERT INTO "Assignments" ("title","created","due","tokenNum","tokenType","submission","ClassId") VALUES ('Ridiculously Impossible Problem','2014-02-12', '2014-03-14',3,'total',true,2);
	INSERT INTO "AssignmentPrompts" ("DocumentId", "AssignmentId") VALUES (8,5);
	INSERT INTO "AssignmentAttempts" ("timestamp", "grade", "AssignmentId", "UserId") VALUES ('2014-03-05',30,5,4);
	INSERT INTO "AssignmentAttempts" ("timestamp", "grade", "AssignmentId", "UserId") VALUES ('2014-03-05',17,5,5);

	INSERT INTO "Assignments" ("title","created","due","tokenNum","tokenType","submission","ClassId") VALUES ('Ridiculously Easy Assignment','2014-03-04', '2014-03-17',3,'total',true,1);
	INSERT INTO "AssignmentPrompts" ("DocumentId", "AssignmentId") VALUES (9,6);
	INSERT INTO "AssignmentAttempts" ("timestamp", "grade", "AssignmentId", "UserId", "DocumentId") VALUES ('2014-03-05',101,6,6,15);

	INSERT INTO "Assignments" ("title","created","due","tokenNum","tokenType","submission","ClassId") VALUES ('Final Exam','2014-03-04', '2014-05-09',-1,'total',false,4);
	INSERT INTO "AssignmentPrompts" ("DocumentId", "AssignmentId") VALUES (10,7);

	INSERT INTO "Assignments" ("title","created","due","tokenNum","tokenType","submission","ClassId") VALUES ('Book Problems','2014-03-04', '2014-03-12',3,'total',true,4);
	INSERT INTO "AssignmentPrompts" ("DocumentId", "AssignmentId") VALUES (11,8);
	INSERT INTO "AssignmentAttempts" ("timestamp", "grade", "AssignmentId", "UserId") VALUES ('2014-03-05',77,8,1);
	INSERT INTO "AssignmentAttempts" ("timestamp", "grade", "AssignmentId", "UserId") VALUES ('2014-03-05',79,8,3);
	INSERT INTO "AssignmentAttempts" ("timestamp", "grade", "AssignmentId", "UserId") VALUES ('2014-03-05',65,8,4);
	INSERT INTO "AssignmentAttempts" ("timestamp", "grade", "AssignmentId", "UserId") VALUES ('2014-03-05',98,8,7);

	INSERT INTO "Assignments" ("title","created","due","tokenNum","tokenType","submission","ClassId") VALUES ('Other Ridiculous Program','2014-03-04', '2014-03-14',3,'total',true,2);
	INSERT INTO "AssignmentPrompts" ("DocumentId", "AssignmentId") VALUES (12,9);
	INSERT INTO "AssignmentAttempts" ("timestamp", "grade", "AssignmentId", "UserId") VALUES ('2014-03-05',20,9,4);
	INSERT INTO "AssignmentAttempts" ("timestamp", "grade", "AssignmentId", "UserId") VALUES ('2014-03-05',45,9,5);

	INSERT INTO "Assignments" ("title","created","due","tokenNum","tokenType","submission","ClassId") VALUES ('Presentation 2','2014-03-04', '2014-03-07',-1,'total',false,3);
	INSERT INTO "AssignmentPrompts" ("DocumentId", "AssignmentId") VALUES (13,10);

	INSERT INTO "Posts" ("ClassId","UserId","text", "title", "priority", "timestamp") VALUES (1,2,'This is a post','Test Post','medium','2014-03-26');
	INSERT INTO "Comments" ("UserId", "PostId", "text", "timestamp") VALUES (6,1,'Test response', '2014-03-26');
	INSERT INTO "Comments" ("UserId", "PostId", "text", "timestamp") VALUES (6,1,'Really Really Really Really Really Really Really Really Really Really Really Really Really Really Really Really Really Really Really Really Really Really Really Really Really Really Really Really Really Really Really Really long response', '2014-03-25');

	INSERT INTO "Posts" ("ClassId","UserId","text", "title", "priority", "timestamp") VALUES (1,2,'Warning warning','Test post: the sequel','high','2014-03-27');
	INSERT INTO "Comments" ("UserId", "PostId", "text", "timestamp") VALUES (6,2,'Ahhhh', '2014-03-27');

	INSERT INTO "NotificationTypes" ("name") VALUES ('Grades');
	INSERT INTO "NotificationTypes" ("name") VALUES ('Attendance');
	INSERT INTO "NotificationTypes" ("name") VALUES ('Assignment');
	INSERT INTO "NotificationTypes" ("name") VALUES ('Discussion');

	INSERT INTO "NotificationPreferences" ("ClassId", "UserId", "NotificationTypeId") VALUES (4,1,1);

EOF
