Ivory
=====================
This is the Capstone Project for the Spring 2014 Semester.

We are the Codeslingers, and our product is named Ivory. It is a Learning Management System (LMS) that seeks to bring the LMS into the modern age. Most LMSs are cluttered and confusing, filled with ununsed or half-implemented features. Ivory is the answer to these problems. Ivory is designed to be a simple, Single-Page App that any student or professor could easily use in seconds to do what they need, and not have to wade through multiple pages and tools that they don't need.

#Technologies

We began by basing the project off of [http://mean.io](http://mean.io), and we changed a few things how we saw fit.

In the end, we used [AngularJS](https://angularjs.org/), [NodeJS Express](http://expressjs.com/), [PostgreSQL](http://www.postgresql.org/), [Sequelize](http://sequelizejs.com/), and [Docker](https://www.docker.io/). 