var db = require('./config/sequelize');
var schedule = require('node-schedule');

module.exports = function () {

	Date.prototype.yyyymmdd = function() {         
                                
        var yyyy = this.getFullYear().toString();                                    
        var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based         
        var dd  = this.getDate().toString();             
                            
        return yyyy + '-' + (mm[1]?mm:"0"+mm[0]) + '-' + (dd[1]?dd:"0"+dd[0]);
   }; 



	var rule = new schedule.RecurrenceRule();
	rule.dayOfWeek = [new schedule.Range(1,5)];
	rule.hour = 0;
	rule.minute = 1;

	var job = schedule.scheduleJob(rule, function() {
		var todayDate = new Date();
		var day = todayDate.getDay();
		todayDate = todayDate.yyyymmdd();
		var toUpdate = [];


		switch(day) {
			case 1:
				day = 'M';
				break;
			case 2:
				day = 'T';
				break;
			case 3:
				day = 'W';
				break;
			case 4:
				day = 'R';
				break;
			case 5:
				day = 'F';
				break;
		}

		db.Class.findAll().success(function(classes) {

			for(var classObj in classes) {
				if(classes[classObj].days.indexOf(day) != -1 && classes[classObj].closed == false) {
					toUpdate.push(classes[classObj]);
				}
			}

			for(var classObj in toUpdate) {
				db.ClassUser.findAll({
					where: ['"ClassId"=? and "RoleId"=?', toUpdate[classObj].id, 1]
				}).success(function(classUsers) {
					for(user in classUsers) {
						db.Attendance.create({
							date: todayDate,
							status: 'P',
							ClassId: classUsers[user].ClassId,
							UserId: classUsers[user].UserId
						}).error(function(err) {
							console.log(err);
						});
					}


				}).error(function(err) {
					console.log(err);
				});
			}
		}).error(function(err) {
			console.log(err);
		});


	});

}