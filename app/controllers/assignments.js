var db = require('../../config/sequelize');
var _ = require('lodash');
var rootPath = require('../../config/config').ivoryPath;
var fs = require('fs');
var mkdirp = require('mkdirp');
var restler = require('restler');
var child = require('child_process');

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// GET /api/assignments/:assignmentID
// Gets information about an assignment
exports.getAssignment = function (req, res) {
    db.Assignment.find({
        where: {id: req.params.assignmentID},
        include: [db.AssignmentPrompt]
    }).success(function (assignment) {
        if (assignment)
            db.Document.find(assignment.assignmentPrompt.DocumentId).success(function (doc) {
                res.json({
                    ClassId: assignment.ClassId,
                    title: assignment.title,
                    due: assignment.due,
                    tokenNum: assignment.tokenNum,
                    tokenType: assignment.tokenType,
                    submission: assignment.submission,
                    promptText: doc.text || 'null',
                    promptFile: doc.filePath || 'null'
                });
            }).error(function (err) { console.log(err); res.end('error'); });
        else res.end();
    }).error(function (err) { console.log(err); res.end('error'); });
};

// PUT /api/assignments/:assignmentID
// Updates an assignment with any information that is present
exports.updateAssignment = function (req, res) {
    db.Assignment.find(req.params.assignmentID).success(function (assignment) {
        for (field in req.body) {
            assignment[field] = req.body[field];
        }

        assignment.save()
            .success(function () { res.end(); })
            .error(function (err) { console.log(err); res.end('error'); });
    }).error(function (err) { console.log(err); res.end('error'); });
};

// DELETE /api/assignments/:assignmentID
// Deletes an assignment
exports.deleteAssignment = function (req, res) {
    db.Assignment.find(req.params.assignmentID).success(function (assignment) {
        assignment.destroy()
            .success(function () { res.end(); })
            .error(function (err) { console.log(err); res.end('error'); });
    }).error(function (err) { console.log(err); res.end('error'); });
};

// GET /api/assignments/class/:classID/
// Gets all assignments for a class
// TODO: add date limiting
exports.getClassAssignments = function (req, res) {
    var classID = req.params.classID;
    var results = [];

    db.Assignment.findAll({
        where: {ClassId: classID},
        include: [db.AssignmentPrompt]
    }).success(function (assignments) {
        var sendJSON = _.after(assignments.length, function () {
            res.json(results);
        });

        _.each(assignments, function (assignment) {
            db.Document.find(assignment.assignmentPrompt.DocumentId).success(function (doc) {
                results.push({
                    id: assignment.id,
                    ClassId: classID,
                    title: assignment.title,
                    due: assignment.due,
                    tokenNum: assignment.tokenNum,
                    tokenType: assignment.tokenType,
                    submission: assignment.submission,
                    promptText: doc.text || 'null',
                    promptFile: doc.filePath || 'null',
                    programID: assignment.ProgramId
                });
            }).then(sendJSON);
        });
    });
};

// POST /api/assignments/class/:classID
// Adds an assignment for a class
exports.addClassAssignment = function (req, res) {
    var file = req.files.uploadedFile;
    var assignmentVar;

    if(file != undefined)  {
        var curTime = new Date();
        var fileName = req.user.id + '_' + curTime.valueOf() + '_' + req.files.uploadedFile.name;
        var oldPath = req.files.uploadedFile.path;
        var newPath = rootPath + '/uploads/' + fileName;
    }

    if(file != undefined) {
         fs.readFile(oldPath, function (err, data) {
            if (err) console.log(err);
            fs.writeFile(newPath, data, function (err) {
                if (err) console.log(err);

                db.Assignment.create({
                    title: req.body.title,
                    created: new Date(),
                    due: req.body.due,
                    tokenNum: req.body.tokenNum,
                    tokenType: req.body.tokenType,
                    submission: req.body.submission,
                    ClassId: req.params.classID
                }).success(function (assignment) {
                    assignmentVar = assignment;
                    db.Document.create({
                        text: req.body.promptText,
                        filePath: '/uploads/' + fileName 
                    }).success(function (document) {
                        db.AssignmentPrompt.create({
                            DocumentId: document.id,
                            AssignmentId: assignment.id
                        }).success(function () { 

                            
                            if(req.body.code === "true") {
                                if(req.body.inputBool === "false") {
                                    db.Program.create({
                                        name: req.body.title,
                                        language: req.body.programLang.toLowerCase(),
                                        checkboxes: "temp",
                                        memory: "-m " + req.body.memory + "m",
                                        diskSpace: req.body.HDD + "M",
                                        runLimit: req.body.runTime + "s",
                                        accuracy: req.body.accuracy,
                                        input: false,
                                        testFile: null,
                                        outputFile: null
                                    }).success(function (prog) {

                                        assignmentVar.ProgramId = prog.id;
                                        assignmentVar.save().error(function (err) { console.log(err); res.end('error'); });

                                        child.spawn('curl', ['--data', 'programID=' + prog.id, 'localhost:7001/create']);

                                        db.ClassUser.findAll({where: {ClassId: req.params.classID}}).success(function(classusers) {
                                            for (var user in classusers) {
                                                if(classusers[user].RoleId == 1) {
                                                    var notifyText = 'New Assignment in class ' + req.params.classID;
                                                    var newNotify = db.Notification.build({
                                                        UserId: classusers[user].UserId,
                                                        ClassId: req.params.classID,
                                                        NotificationTypeId: 3,
                                                        title: 'New Assignment', 
                                                        text: notifyText,
                                                        toEmail: true
                                                    }).save().error(function(err) {
                                                        console.log(err);
                                                        res.end();
                                                    })
                                                }
                                            }

                                            res.end();
                                        }).error(function (err) { console.log(err); res.end('error'); });

                                    }).error(function(err) {
                                        console.log(err);
                                        res.end();
                                    });
                                }

                                else {
                                    var curTime = new Date();
                                    var inFileName = req.files.inputFile.name;
                                    var inExtension = inFileName.split('.').pop();
                                    console.log(inExtension)
                                    if(inExtension != 'zip' && inExtension != 'in'){
                                        inFileName = inFileName.replace('.'+inExtension, '.in');
                                        console.log(inFileName);
                                    }
                                    var inOldPath = req.files.inputFile.path;
                                    //var inNewPath = rootPath + '/public/TUSKFILES/programs/' + inFileName; 
                                    

                                    curTime = new Date();   
                                    var outFileName = req.files.inputFile.name;
                                    var outExtension = outFileName.split('.').pop();
                                    if(outExtension != 'zip' && outExtension != 'ans'){
                                        outFileName = outFileName.replace('.'+outExtension, '.ans');
                                        console.log(outFileName);
                                    }
                                    var outOldPath = req.files.outputFile.path;
                                    //var outNewPath = rootPath + '/public/files/' + outFileName; 

                                    var inDoc;
                                    var outDoc;

                                    db.Document.create({
                                        text: null,
                                        filePath: 'temp'
                                    }).success(function (document) {
                                        var inDoc = document;
                                        db.Document.create({
                                            text: null,
                                            filePath: 'temp'
                                        }).success(function (document) {
                                            var outDoc = document;
                                            db.Program.create({
                                                name: req.body.title,
                                                language: req.body.programLang.toLowerCase(),
                                                checkboxes: "temp",
                                                memory: "-m " + req.body.memory + "m",
                                                diskSpace: req.body.HDD + "M",
                                                runLimit: req.body.runTime + "s",
                                                accuracy: req.body.accuracy,
                                                input: true,
                                                testFile: inDoc.id,
                                                outputFile: document.id
                                            }).success(function (prog) {

                                                var inNewPath = rootPath + '/tusk/programs/' + prog.id + '/data/' + inFileName;
                                                var outNewPath = rootPath + '/tusk/programs/' + prog.id + '/data/' + outFileName; 

                                                mkdirp(rootPath + '/tusk/programs/' + prog.id + '/data/', function(err) {
                                                    if(err) console.log(err);
                                                    fs.readFile(inOldPath, function (err, inData) {
                                                        if (err) console.log(err);
                                                        fs.writeFile(inNewPath, inData, function (err) {
                                                            if (err) console.log(err);
                                                            fs.readFile(outOldPath, function (err, outData) {
                                                                if (err) console.log(err);
                                                                fs.writeFile(outNewPath, outData, function (err) {
                                                                    if (err) console.log(err);

                                                                });
                                                            });
                                                        });
                                                    });
                                                });

                                                inDoc.filePath = inNewPath;
                                                outDoc.filePath = outNewPath;

                                                inDoc.save().error(function (err) { console.log(err); res.end('error'); });
                                                outDoc.save().error(function (err) { console.log(err); res.end('error'); });


                                                assignmentVar.ProgramId = prog.id;
                                                assignmentVar.save().error(function (err) { console.log(err); res.end('error'); });

                                                child.spawn('curl', ['--data', 'programID=' + prog.id, 'localhost:7001/create']);
                                               //restler.post('localhost:8080/create', {
                                                    //data: {
                                                        //"programID": prog.id 
                                                    //}
                                                //}).on("complete", function(result) {
                                                    //if(result instanceof Error) {
                                                        //console.log(result.message);
                                                    //}
                                                    //else {
                                                        //console.log("It worked!");
                                                    //}   
                                                //});

                                                db.ClassUser.findAll({where: {ClassId: req.params.classID}}).success(function(classusers) {
                                                    for (var user in classusers) {
                                                        if(classusers[user].RoleId == 1) {
                                                            var notifyText = 'New Assignment in class ' + req.params.classID;
                                                            var newNotify = db.Notification.build({
                                                                UserId: classusers[user].UserId,
                                                                ClassId: req.params.classID,
                                                                NotificationTypeId: 3,
                                                                title: 'New Assignment', 
                                                                text: notifyText,
                                                                toEmail: true
                                                            }).save().error(function(err) {
                                                                console.log(err);
                                                                res.end();
                                                            })
                                                        }
                                                    }

                                                    res.end();
                                                }).error(function (err) { console.log(err); res.end('error'); });

                                            }).error(function(err) {
                                                console.log(err);
                                                res.end();
                                            });
                                        }).error(function(err) {
                                            console.log(err);
                                            res.end();
                                        });
                                    }).error(function(err) {
                                        console.log(err);
                                        res.end(); 
                                    })

                                    
                                    //zip stuff

                                    res.end();
                                }
                            }

                            else {
                                db.ClassUser.findAll({where: {ClassId: req.params.classID}}).success(function(classusers) {
                                    for (var user in classusers) {
                                         if(classusers[user].id == 1) {
                                            var notifyText = 'New Assignment in class ' + req.params.classID;
                                            var newNotify = db.Notification.build({
                                                UserId: classusers[user].UserId,
                                                ClassId: req.params.classID,
                                                NotificationTypeId: 3,
                                                title: 'New Assignment', 
                                                text: notifyText,
                                                toEmail: true
                                            }).save().error(function(err) {
                                                console.log(err);
                                                res.end();
                                            })
                                        }
                                    }

                                    res.end();
                                }).error(function (err) { console.log(err); res.end('error'); });
                            }
                        })
                        .error(function (err) { console.log(err); res.end('error'); });
                    }).error(function (err) { console.log(err); res.end('error'); });
                }).error(function (err) { console.log(err); res.end('error'); });
            });
        });
    } else {
        db.Assignment.create({
            title: req.body.title,
            created: new Date(),
            due: req.body.due,
            tokenNum: req.body.tokenNum,
            tokenType: req.body.tokenType,
            submission: req.body.submission,
            ClassId: req.params.classID
        }).success(function (assignment) {
            assignmentVar = assignment;
            db.Document.create({
                text: req.body.promptText,
                filePath: null
            }).success(function (document) {
                db.AssignmentPrompt.create({
                    DocumentId: document.id,
                    AssignmentId: assignment.id
                }).success(function () { 
                    if(req.body.code === "true") {
                        if(req.body.inputBool === "false") {
                            db.Program.create({
                                name: req.body.title,
                                language: req.body.programLang.toLowerCase(),
                                checkboxes: "temp",
                                memory: "-m " + req.body.memory + "m",
                                diskSpace: req.body.HDD + "M",
                                runLimit: req.body.runTime + "s",
                                accuracy: req.body.accuracy,
                                input: false,
                                testFile: null,
                                outputFile: null
                            }).success(function (prog) {

                                assignmentVar.ProgramId = prog.id;
                                assignmentVar.save().error(function (err) { console.log(err); res.end('error'); });

                                child.spawn('curl', ['--data', 'programID=' + prog.id, 'localhost:7001/create']);

                                db.ClassUser.findAll({where: {ClassId: req.params.classID}}).success(function(classusers) {
                                    for (var user in classusers) {
                                         if(classusers[user].RoleId == 1) {
                                            var notifyText = 'New Assignment in class ' + req.params.classID;
                                            var newNotify = db.Notification.build({
                                                UserId: classusers[user].UserId,
                                                ClassId: req.params.classID,
                                                NotificationTypeId: 3,
                                                title: 'New Assignment', 
                                                text: notifyText,
                                                toEmail: true
                                            }).save().error(function(err) {
                                                console.log(err);
                                                res.end();
                                            })
                                        }
                                    }

                                    res.end();
                                }).error(function (err) { console.log(err); res.end('error'); });

                            }).error(function(err) {
                                console.log(err);
                                res.end();
                            });
                        }

                        else {
                            var curTime = new Date();
                            var inFileName = req.files.inputFile.name;
                            var inExtension = inFileName.split('.').pop();
                            console.log(inExtension)
                            if(inExtension != 'zip' && inExtension != 'in'){
                                inFileName = inFileName.replace('.'+inExtension, '.in');
                                console.log(inFileName);
                            }
                            var inOldPath = req.files.inputFile.path;
                            //var inNewPath = rootPath + '/public/TUSKFILES/programs/' + inFileName; 
                            

                            curTime = new Date();   
                            var outFileName = req.files.inputFile.name;
                            var outExtension = outFileName.split('.').pop();
                            if(outExtension != 'zip' && outExtension != 'ans'){
                                outFileName = outFileName.replace('.'+outExtension, '.ans');
                                console.log(outFileName);
                            }
                            var outOldPath = req.files.outputFile.path;
                            //var outNewPath = rootPath + '/public/files/' + outFileName; 

                            var inDoc;
                            var outDoc;

                            db.Document.create({
                                text: null,
                                filePath: 'temp'
                            }).success(function (document) {
                                var inDoc = document;
                                db.Document.create({
                                    text: null,
                                    filePath: 'temp'
                                }).success(function (document) {
                                    var outDoc = document;
                                    db.Program.create({
                                        name: req.body.title,
                                        language: req.body.programLang.toLowerCase(),
                                        checkboxes: "temp",
                                        memory: "-m " + req.body.memory + "m",
                                        diskSpace: req.body.HDD + "M",
                                        runLimit: req.body.runTime + "s",
                                        accuracy: req.body.accuracy,
                                        input: true,
                                        testFile: inDoc.id,
                                        outputFile: document.id
                                    }).success(function (prog) {

                                        var inNewPath = rootPath + '/tusk/programs/' + prog.id + '/data/' + inFileName;
                                        var outNewPath = rootPath + '/tusk/programs/' + prog.id + '/data/' + outFileName; 

                                        mkdirp(rootPath + '/tusk/programs/' + prog.id + '/data/', function(err) {
                                            if(err) console.log(err);
                                            fs.readFile(inOldPath, function (err, inData) {
                                                if (err) console.log(err);
                                                fs.writeFile(inNewPath, inData, function (err) {
                                                    if (err) console.log(err);
                                                    fs.readFile(outOldPath, function (err, outData) {
                                                        if (err) console.log(err);
                                                        fs.writeFile(outNewPath, outData, function (err) {
                                                            if (err) console.log(err);

                                                        });
                                                    });
                                                });
                                            });
                                        });

                                        inDoc.filePath = inNewPath;
                                        outDoc.filePath = outNewPath;

                                        inDoc.save().error(function (err) { console.log(err); res.end('error'); });
                                        outDoc.save().error(function (err) { console.log(err); res.end('error'); });


                                        assignmentVar.ProgramId = prog.id;
                                        assignmentVar.save().error(function (err) { console.log(err); res.end('error'); });

                                        child.spawn('curl', ['--data', 'programID=' + prog.id, 'localhost:7001/create']);
                                       /*restler.post('localhost:8080/create', {
                                            data: {
                                                "programID": prog.id 
                                            }
                                        }).on("complete", function(result) {
                                            if(result instanceof Error) {
                                                console.log(result.message);
                                            }
                                            else {
                                                console.log("It worked!");
                                            }   
    
                                        });*/

                                        db.ClassUser.findAll({where: {ClassId: req.params.classID}}).success(function(classusers) {
                                            for (var user in classusers) {
                                                if(classusers[user].RoleId == 1) {
                                                    var notifyText = 'New Assignment in class ' + req.params.classID;
                                                    var newNotify = db.Notification.build({
                                                        UserId: classusers[user].UserId,
                                                        ClassId: req.params.classID,
                                                        NotificationTypeId: 3,
                                                        title: 'New Assignment', 
                                                        text: notifyText,
                                                        toEmail: true
                                                    }).save().error(function(err) {
                                                        console.log(err);
                                                        res.end();
                                                    })
                                                }
                                            }

                                            res.end();
                                        }).error(function (err) { console.log(err); res.end('error'); });

                                    }).error(function(err) {
                                        console.log(err);
                                        res.end();
                                    });
                                }).error(function(err) {
                                    console.log(err);
                                    res.end();
                                });
                            }).error(function(err) {
                                console.log(err);
                                res.end(); 
                            })

                            
                            //zip stuff

                            res.end();
                        }
                    }



                    else {
                        db.ClassUser.findAll({where: {ClassId: req.params.classID}}).success(function(classusers) {
                            for (var user in classusers) {
                                 if(classusers[user].RoleId == 1) {
                                    var notifyText = 'New Assignment in class ' + req.params.classID;
                                    var newNotify = db.Notification.build({
                                        UserId: classusers[user].UserId,
                                        ClassId: req.params.classID,
                                        NotificationTypeId: 3,
                                        title: 'New Assignment', 
                                        text: notifyText,
                                        toEmail: true
                                    }).save().error(function(err) {
                                        console.log(err);
                                        res.end();
                                    })
                                }
                            }
                        res.end(); 
                        }).error(function (err) { console.log(err); res.end('error'); });
                    }

                    
                }).error(function (err) { console.log(err); res.end('error'); });
            }).error(function (err) { console.log(err); res.end('error'); });
        }).error(function (err) { console.log(err); res.end('error'); });
    }
};

//POST /api/assignments/attempt/:assignmentID
//Adds a student submission for an assignment
exports.submitAttempt = function(req,res) {

    var file = req.files.uploadedFile;
    var curTime = new Date();
    var fileName = req.user.id + '_' + curTime.valueOf() + '_' + req.files.uploadedFile.name;
    var oldPath = req.files.uploadedFile.path;
    var newPath = rootPath + '/uploads/' + fileName;
    var attemptId;

    fs.readFile(oldPath, function (err, data) {
        if (err) console.log(err);
        fs.writeFile(newPath, data, function (err) {
            if (err) console.log(err);


            db.AssignmentAttempt.find({
                where: ['"UserId"=? and "AssignmentId"=?',req.user.id, req.params.assignmentID]
            }).success(function(attempt) {

                if(attempt != null) {
                    attemptId = attempt.id;
                    if(attempt.DocumentId != null && attempt.DocumentId != undefined) {
                        db.Document.find({where: {id: attempt.DocumentId}}).success(function(doc) {
                            doc.filePath = '/uploads/' + fileName;
                            doc.save().success(function() {
                                if(req.body.programID != null && req.body.programID != "null" && req.body.programID != '0' && req.body.programID != 0) {
                                var username = req.user.name;
                                username = username.replace(/ /g,"_");
                                username = username.toLowerCase();

                                var newNewPath = rootPath + '/tusk/user_folders/' + username + '/' + req.body.programID + '/';

                                fs.readFile(oldPath, function (err, data) {
                                    if (err) console.log(err);
                                    mkdirp(newNewPath, function(err) {
                                        if(err) console.log(err);
                                    
                                        fs.writeFile(newNewPath+req.files.uploadedFile.name, data, function (err) {
                                            if (err) console.log(err);
                                            db.Execution.create({
                                                ProgramId: req.body.programID,
                                                UserId: req.user.id,
                                                status: '0'
                                            }).success(function(exec) {
                                            child.spawn('curl', ['--data', 'mainFile=' + req.files.uploadedFile.name + '&exID=' + exec.id + '&attemptID=' + attemptId, 'localhost:7001/run']);

                                                /*restler.post('localhost:8080/run', {
                                                    data: {
                                                        "mainFile": req.body.mainFile,
                                                        "exID": exec.id,
                                                        "attemptID": attemptId
                                                    }
                                                }).on("complete", function(result) {
                                                    if(result instanceof Error) {
                                                        console.log(result.message);
                                                    }
                                                    else {
                                                        console.log("It worked!");
                                                    }   

                                                });*/



                                                res.end(exec.id.toString());
                                            }).error(function(err) {
                                                console.log(err);
                                                res.end(err);
                                            });
                                        });
                                    });
                                });

                            }
                            else {
                                res.end("Created");
                            }
                            }).error(function(err) {
                                console.log(err);
                                res.end("Could not create");
                            });

                        }).error(function(err) {
                            console.log(err);
                            res.end("Could not create");
                        });
                    }
                    else {
                        db.Document.create({filePath: '/uploads/' + fileName}).success(function(doc) {
                            attempt.DocumentId = doc.id;
                            attempt.save().success(function() {
                                if(req.body.programID != null  && req.body.programID != "null"  && req.body.programID != '0' && req.body.programID != 0) {
                                var username = req.user.name;
                                username = username.replace(/ /g,"_");
                                username = username.toLowerCase();

                                var newNewPath = rootPath + '/tusk/user_folders/' + username + '/' + req.body.programID + '/';

                                fs.readFile(oldPath, function (err, data) {
                                    if (err) console.log(err);
                                    mkdirp(newNewPath, function(err) {
                                        if(err) console.log(err);
                                    
                                        fs.writeFile(newNewPath+req.files.uploadedFile.name, data, function (err) {
                                            if (err) console.log(err);
                                            db.Execution.create({
                                                ProgramId: req.body.programID,
                                                UserId: req.user.id,
                                                status: '0'
                                            }).success(function(exec) {
                                                 child.spawn('curl', ['--data', 'mainFile=' + req.files.uploadedFile.name + '&exID=' + exec.id + '&attemptID=' + attemptId, 'localhost:7001/run']);


                                                /*restler.post('localhost:8080/run', {
                                                    data: {
                                                        "mainFile": req.body.mainFile,
                                                        "exID": exec.id,
                                                        "attemptID": attemptId
                                                    }
                                                }).on("complete", function(result) {
                                                     if(result instanceof Error) {
                                                        console.log(result.message);
                                                    }
                                                    else {
                                                        console.log("It worked!");
                                                    }   
                                                   
                                                });*/

                                                res.end(exec.id.toString());
                                            }).error(function(err) {
                                                console.log(err);
                                                res.end(err);
                                            });
                                        });
                                    });
                                });

                            }
                            else {
                                res.end("Created");
                            }
                            }).error(function(err) {
                                console.log(err);
                                res.end("Could not create");
                            });


                        }).error(function(err) {
                            console.log(err);
                            res.end("Could not create");
                        });
                    }
                }
                else {
                    db.Document.create({filePath: '/uploads/' + fileName}).success(function(doc) {

                        db.AssignmentAttempt.create({
                            AssignmentId: req.params.assignmentID,
                            UserId: req.user.id,
                            DocumentId: doc.id
                        }).success(function(attempt) {
                            attemptId = attempt.id;
                            if(req.body.programID != null  && req.body.programID != "null") {
                                var username = req.user.name;
                                username = username.replace(/ /g,"_");
                                username = username.toLowerCase();

                                var newNewPath = rootPath + '/tusk/user_folders/' + username + '/' + req.body.programID + '/';

                                fs.readFile(oldPath, function (err, data) {
                                    if (err) console.log(err);
                                    mkdirp(newNewPath, function(err) {
                                        if(err) console.log(err);
                                    
                                        fs.writeFile(newNewPath+req.files.uploadedFile.name, data, function (err) {
                                            if (err) console.log(err);
                                            db.Execution.create({
                                                ProgramId: req.body.programID,
                                                UserId: req.user.id,
                                                status: '0'
                                            }).success(function(exec) {
                                                 child.spawn('curl', ['--data', 'mainFile=' + req.files.uploadedFile.name + '&exID=' + exec.id + '&attemptID=' + attemptId, 'localhost:7001/run']);


                                                /*restler.post('localhost:8080/run', {
                                                    data: {
                                                        "mainFile": req.body.mainFile,
                                                        "exID": exec.id,
                                                        "attemptID": attemptId
                                                    }
                                                }).on("complete", function(result) {
                                                     if(result instanceof Error) {
                                                        console.log(result.message);
                                                    }
                                                    else {
                                                        console.log("It worked!");
                                                    }   
                                                   
                                                });*/

                                                res.end(exec.id.toString());
                                            }).error(function(err) {
                                                console.log(err);
                                                res.end(err);
                                            });
                                        });
                                    });
                                });

                            }
                            else {
                                res.end("Created");
                            }
                        }).error(function(err) {
                            console.log(err);
                            res.end("Could not create");
                        })

                    }).error(function(err) {
                        console.log(err);
                        res.end("Could not create");
                    });

                }

            }).error(function(err) {
                console.log(err);
            });

        });
    });

};

exports.getExecution = function(req,res) {

    var response;
    var executionVar;

    db.Execution.find({
        where: ['"UserId"=? and id=? and status=?',req.user.id, req.params.exID,'1']
    }).success(function(execution) {
        if(execution != null) {
            executionVar = execution;
            db.Document.find({where: {id: execution.outputFile}}).success(function(doc) {
                response = {
                    status: executionVar.status,
                    timeSubmitted: executionVar.timeSubmitted,
                    outputFile: doc.filePath
                };
                
                executionVar.status = '2';
                executionVar.save().success(function() {
                    res.json(response); 
                }).error(function(err) {
                    console.log(err);
                });


            }).error(function(err) {
                console.log(err);
                res.end();
            })
          
        }
        else {
            res.end(null);
        }
        
    }).error(function(err) {
        console.log(err);
        res.end();
    });

};
