var db = require('../../config/sequelize');
var moment = require('moment');
var _ = require('lodash');

// Just sends attendance as json
exports.stuAttend = function(req, res) {
    //takes in a userID and a classID
    var userID = req.user.id;
    var classID = req.params.classId;
    var response = [];

    //searches attendances where user and class IDs match
    db.Attendance.findAll({
        where: ['"UserId"=? and "ClassId"=?',userID, classID]
    }).success(function(attendances) {
        for(var i=0;i<attendances.length;i++) {
            response.push({
                date: attendances[i].date,
                status: attendances[i].status,
                classId: classID,
                userId: userID
            })
        }
        res.jsonp(response);
    }).error(function(err) {
        console.log(err);
    });
};


//TEMPORARY, PRETTY AWFUL
exports.teachAttend = function(req,res) {
    var userID = req.params.userId;
    var classID = req.params.classId;
    var userName;
    var response = [];

    //searches attendances where user and class IDs match
    db.User.find(userID).success(function(user) {
        userName = user.name;
    }).error(function(err) {
        console.log(err);
    }).then(function() {
        db.Attendance.findAll({
            where: ['"UserId"=? and "ClassId"=?',userID, classID]
        }).success(function(attendances) {
            for(var i=0;i<attendances.length;i++) {
                response.push({
                    name: userName,
                    date: attendances[i].date,
                    status: attendances[i].status,
                    classId: classID,
                    userId: userID
                })
            }
            res.jsonp(response);

        }).error(function(err) {
            console.log(err);
        });
    });

};

exports.postAttend = function(req,res) {

    var userID = req.params.userId;
    var classID = req.params.classId;
    var status = req.params.status;
    var date = req.params.date;

    var newAttend = db.Attendance.build({
        date: date,
        status: status,
        UserId: userID,
        ClassId: classID
    });

    newAttend.save().error(function(err) {
        console.log(err);
    });


    var notifyText = 'Attendance in class ' + classID + ' updated';

    var newNotify = db.Notification.build({
        UserId: userID,
        ClassId: classID,
        NotificationTypeId: 2,
        title: 'Updated Attendance',
        text: notifyText,
        toEmail: true
    }).save().error(function(err) {
        console.log(err);
    })


    res.end();
};

exports.updateAttend = function(req,res) {

    var userID = req.params.userId;
    var classID = req.params.classId;
    var status = req.params.status;
    var date = req.params.date;


    db.Attendance.find({
        where: ['"UserId"=? and "ClassId"=? and date=?',userID, classID, date]
    }).success(function(attendance) {
        attendance.updateAttributes({
            status: status
        }).success(function() {
            res.end();
        }).error(function(err) {
            console.log(err);
        });

    }).error(function(err) {
        console.log(err);
    });

    var notifyText = 'Attendance in class ' + classID + ' updated';

    var newNotify = db.Notification.build({
        UserId: userID,
        ClassId: classID,
        NotificationTypeId: 2,
        title: 'Updated Attendance',
        text: notifyText,
        toEmail: true
    }).save().error(function(err) {
        console.log(err);
    })


};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// GET /api/attendance/class/:classID
// Optional parameters: date=YYYY-MM-DD
// Gets all attendance records for a class, optionally limited by date
exports.getClassAttendance = function (req, res) {
    response = [];

    db.Attendance.findAll({
        where: {
            ClassId: req.params.classID
        }
    }).success(function (attendances) {
        var sendJSON = _.after(attendances.length, function () {
            res.json(response);
        });

        _.forEach(attendances, function (attend) {
            if (req.query.date == undefined || attend.date.indexOf(req.query.date) == 0) {
                response.push({
                    date: attend.date,
                    status: attend.status,
                    classId: attend.ClassId,
                    userId: attend.UserId
                });
            }
            sendJSON();
        });
    }).error(function (err) { console.log(err); res.end('error'); });
};

// GET /api/attendance/class/:classID/user/:userID
// Optional parameters: date=YYYY-MM-DD
// Gets all attendance records for a user in a class, optionally limited by date
exports.getStudentAttendance = function (req,res) {
    var userName;


    db.User.find(req.params.userID).success(function(user) {
        userName = user.name;
    }).error(function(err) {
        console.log(err);
    }).then(function() {
        db.Attendance.findAll({
            where: ['"UserId"=? and "ClassId"=?', req.params.userID, req.params.classID]
        }).success(function(attendances) {
            var response = [];
            for(var i = 0; i < attendances.length; i++) {
                var attend = attendances[i];
                if (req.query.date == undefined || attend.date.indexOf(req.query.date) == 0) {
                    response.push({
                        name: userName,
                        date: attend.date,
                        status: attend.status,
                        classId: req.params.classID,
                        userId: req.params.userID
                    });
                }
            }
            res.json(response);
        }).error(function (err) { console.log(err); res.end('error'); });
    });
};

// POST /api/attendance/class/:classID/user/:userID
// Mandatory parameters: date=YYYY-MM-DD, status=STATUS
// Either creates or modifies an attendance record for a student in a class on a date
exports.addOrUpdateUserAttendance = function(req,res) {
    db.Attendance.findOrCreate({
        date: req.body.date,
        UserId: req.params.userID,
        ClassId: req.params.classID
    }).success(function (record, created) {
        record.status = req.body.status;
        record.save()
            .success(function(){})
            .error(function (err) { console.log(err); res.end('error'); });
        res.end();
    }).error(function (err) { console.log(err); res.end('error'); });
};
