exports.index = function(req, res) {
    if (req.isAuthenticated()) {
        res.redirect('/app');
    } else {
        res.redirect('/login');
    }
};

exports.login = function(req, res) {
    res.sendfile('public/page-login.html');
};

exports.app = function(req, res) {
    if (req.isAuthenticated()) {
        res.sendfile('public/page-dashboard.html');
    } else {
        res.redirect('/');
    }
};

exports.signup = function(req, res) {
    res.sendfile('public/page-signup.html');
};

exports.admin = function(req, res) {
    res.sendfile('public/page-admin.html');
};

exports.dashboard = function(req, res) {
    res.sendfile('public/page-dashboard.html');
}
