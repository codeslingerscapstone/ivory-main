var db = require('../../config/sequelize');

exports.getComments = function(req,res) {

    var postID = req.params.postId;
    var response = [];

    db.Comment.findAll({where: {PostId: postID}}).success(function(comments) {
        for(comment in comments) {
            response.push( {
                text: comments[comment].text,
                timestamp: comments[comment].timestamp,
                userID: comments[comment].UserId,
                postID: comments[comment].PostId,
                id: comments[comment].id,
                author: comments[comment].author
            });
        }

        res.jsonp(response);

    }).error(function(err) {
        console.log(err);
    });

};


exports.newComment = function(req,res) {

    var userID = req.user.id;
    var postID = req.params.postID;

    db.Comment.create({
        UserId: userID,
        PostId: postID, 
        text: req.body.text, 
        author: req.body.author
    }).success(function() {
        db.ClassUser.findAll({where: {ClassId: req.body.classID}}).success(function(classusers) {
            for(var user in classusers) {
                if(classusers[user].RoleId == 1) {
                    var notifyText = 'New comment in class ' + req.params.classID;
                    var newNotify = db.Notification.build({
                        UserId: classusers[user].UserId,
                        ClassId: parseInt(req.body.classID),
                        NotificationTypeId: 4,
                        title: 'New Comment', 
                        text: notifyText,
                        toEmail: true
                    }).save().error(function(err) {
                        console.log(err);
                        res.end();
                    })
                }
            }
            res.end();


        }).error(function(err) {
            console.log(err);
            res.end(err);
        })
    }).error(function(err) {
        console.log(err);
        res.end(err);
    });

};


exports.deleteComment = function(req,res) {

    db.Comment.find({where: {id: req.params.commentID}}).success(function(comment) {
        comment.destroy()
         .success(function() {
            res.end();
         })
         .error(function(err) {
            res.end();
            console.log(err);
         });

    }).error(function(err) {
        res.end();
        console.log(err);
    });
};