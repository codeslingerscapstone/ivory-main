var db = require('../../config/sequelize');
var lodash = require('lodash');
var rootPath = require('../../config/config').ivoryPath;
var fs = require('fs');

// GET
exports.getSyllabus = function(req,res) {
    var classID = req.params.classID;

	var classID = req.params.classId;

	db.Syllabus.find({ where: {ClassId: classID}}).success(function(syllabus) {
		if(syllabus == null) {
			res.end();
		}
		else {
			db.Document.find({where : {id: syllabus.DocumentId}}).success(function(doc) {
				var returnDoc = {
					text: doc.text,
					file: doc.filePath,
					classID: classID
				};
				res.jsonp(returnDoc);

			}).error(function(err) {
				console.log(err);
			}); 
		}

    }).error(function(err) {
        console.log(err);
        res.end();
    });
}

exports.uploadSyllabus = function (req, res) {
    var file = req.files.uploadedFile;

    if(file != undefined)  {
        var curTime = new Date();
        var fileName = req.user.id + '_' + curTime.valueOf() + '_' + req.files.uploadedFile.name;
        var oldPath = req.files.uploadedFile.path;
        var newPath = rootPath + '/uploads/' + fileName;
    }


    db.Syllabus.find({ where: {ClassId: req.params.classID}}).success(function(syllabus) {
        if (syllabus) {
            if(file != undefined) {
                fs.readFile(oldPath, function (err, data) {
                    if (err) console.log(err);
                    fs.writeFile(newPath, data, function (err) {
                        if (err) console.log(err);

                        db.Document.find(syllabus.DocumentId).success(function (doc) {
                            doc.text = req.body.text || doc.text;
                            doc.filePath = "/uploads/" + fileName;

                            doc.save().success(function () {
                                res.end('syllabus updated');
                            }).error(function (err) {
                                console.log(err);
                                res.end('error');
                            });
                        });
                    });
                });
            }

            else {
                db.Document.find(syllabus.DocumentId).success(function (doc) {
                    doc.text = req.body.text || doc.text;
                    doc.filePath = null;

                    doc.save().success(function () {
                        res.end('syllabus updated');
                    }).error(function (err) {
                        console.log(err);
                        res.end('error');
                    });
                });

            }

        } else {
            if(file != undefined) {
                fs.readFile(oldPath, function (err, data) {
                    if (err) console.log(err);
                    fs.writeFile(newPath, data, function (err) {
                        if (err) console.log(err);

                        db.Document.create({
                            text: req.body.text,
                            filePath: "/uploads/" + fileName,
                        }).success(function (doc) {
                            db.Syllabus.create({
                                ClassId: req.params.classID,
                                DocumentId: doc.id
                            }).success(function (syll) {
                                res.end('successfully saved');
                            }).error(function (err) { console.log(err); res.end(err); });
                        }).error(function (err) { console.log(err); res.end(err); });
                    });
                });
            }
            else {
                db.Document.create({
                    text: req.body.text,
                    filePath: null,
                }).success(function (doc) {
                    db.Syllabus.create({
                        ClassId: req.params.classID,
                        DocumentId: doc.id
                    }).success(function (syll) {
                        res.end('successfully saved');
                    }).error(function (err) { console.log(err); res.end(err); });
                }).error(function (err) { console.log(err); res.end(err); });
            }
        }
    }
)}
