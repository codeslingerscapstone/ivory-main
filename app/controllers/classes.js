var db = require('../../config/sequelize');

exports.classObj = function(req, res, next, id) {
    db.Class.find({where : { id: id }}).success(function(user){
      if (!user) return next(new Error('Failed to load User ' + id));
      req.profile = user;
      next();
    }).error(function(err){
      next(err);
    });
};

exports.allUsersForProf = function(req, res) {
    var userID = req.user.id;
    var profRole = 2;
    var stuRole = 1;
    var index = 0;
    var response = [];


    db.ClassUser.findAll({
        where: ['"UserId"=? and "RoleId"=?',userID, profRole]
    }).success(function(classUsers) {
        var length = classUsers.length;
        for(classUser in classUsers) {
            db.ClassUser.findAll({
                where: ['"ClassId"=? and "RoleId"=?',classUsers[classUser].ClassId, stuRole]
            }).success(function(students) {
                var IDs = [];
                for(student in students) {
                    var stud = students[student].UserId;
                    IDs.push(stud);
                }
                response.push({
                    classID: classUsers[index].ClassId,
                    IDs: IDs
                });
                index++;

                if(response.length == length) {
                    res.jsonp(response);
                }

            }).error(function(err) {
                console.log(err);
            });
        }
    }).error(function(err) {
        console.log(err);
    });
};

// GET /api/classes/:classID
// Gets information about a class
exports.getClass = function (req, res) {
    // TODO: implement
};

// POST /api/classes/:classID/user/:userID
// Updates a user's role in a class
exports.addOrUpdateClassUserRole = function (req, res) {
    // TODO: implement
};

// GET /api/classes/user/:userID
// Mandatory parameters: role
exports.getClassesForUserWithRole = function (req, res) {
    var specifiedRole = req.query.role;

    if (!specifiedRole) {
        res.end("must pass in a role");
        return;
    }

    var userID = req.params.userID;
    var roleID;
    var classIDs = [];

    db.Role.findAll().success(function (roles) {
        for (var i = 0; i < roles.length; i++) {
            if (roles[i].name == specifiedRole) {
                roleID = roles[i].id;
                break;
            }
        }
    }).then(function () {
        db.ClassUser.findAll({
            where: ['"UserId"=? and "RoleId"=?', userID, roleID]
        }).success(function (classUsers) {
            for (var i = 0; i < classUsers.length; i++) {
                classIDs.push(classUsers[i].ClassId);
            }

            res.json(classIDs);
        }).error(function (err) { console.log(err); res.end('error'); });
    }).error(function (err) { console.log(err); res.end('error'); });
};

exports.allClasses = function(req,res) {
    var response = [];
	var index = 0;
	var outerIndex = 0;
	var associationsArr = [];

	db.Class.findAll().success(function(classes) {
		for(classObj in classes) {
			var students = [];
			var stuIDs = [];
			db.ClassUser.findAll({ where: { ClassId: classes[classObj].id}}).success(function(users) {
     			var classStuIDs = [];
     			var stuCount = 0;
     			for(user in users) {
     				classStuIDs.push(users[user].id.toString());
     				if(users[user].RoleId == 1) {
     					stuCount++;
     				}
     			}
     			students.push(stuCount);
     			stuIDs.push(classStuIDs);
			}).error(function(err) {
				console.log(err);
			}).then(function() {

				db.Course.find({where: {id: classes[outerIndex].id}}).success(function(course) {
					var courseName = course.department + course.number;

					associationsArr[index] = {};
					associationsArr[index]["userIds"] = stuIDs[index];
					associationsArr[index]["courseId"] = course.number;

					response.push({
						id: classes[index].id,
						time: classes[index].time,
						location: classes[index].location,
                        days: classes[index].days,
						course: courseName,
						stuCount: students[index],
						associations: associationsArr[index]
					});
					index++;

					if(index == classes.length) {
						res.json(response);
					}

				}).error(function(err) {
					console.log(err);
				});
				outerIndex++;
			});
		}

	}).error(function(err) {
		console.log(err);
	});
}

exports.allCourses = function(req,res) {
	var response = [];
	var classIds = [];
	var index = 0;
	var associationsArr = [];

	db.Course.findAll().success(function(courses) {

		for(course in courses) {
			db.Class.findAll({where: {CourseId: courses[course].id}})
			.success(function(classes) {
				classIds = [];
				for(classObj in classes) {
					classIds.push(classes[classObj].id);
				}

				associationsArr[index] = {};
				associationsArr[index]["classIds"] = classIds;

				response.push({	
					id: courses[index].id,
					name: courses[index].name,
					department: courses[index].department,
					number: courses[index].number,
					associations: associationsArr[index]
				});
				index++;


				if(index == courses.length) {
					res.json(response);
				}

			}).error(function(err) {
				console.log(err);
			});

		}
    }).error(function(err) {
        console.log(err);
    })
}

exports.addClass = function (req, res) {

};

exports.updateClass = function (req, res) {

};

exports.deleteClass = function (req, res) {

};
