var db = require('../../config/sequelize');




exports.getNotificationPrefs = function (req, res) {
    var classID = req.params.classID;
    var userID = req.user.id;
    var response = [];
    var index = 0;

    db.NotificationPreference.findAll({
        where: ['"UserId"=? and "ClassId"=?',userID, classID]
    }).success(function(prefs) {
        for(pref in prefs) {
            db.NotificationType.find({where: {id:prefs[pref].NotificationTypeId}}).success(function(type) {
                response.push(type.name);
                index++;

                if(index == prefs.length) {
                    res.json(response);    
                }

            }).error(function(err) {
                console.log(err);
            });

        }

        if(prefs.length == 0) {
            res.json(response);
        }


    }).error(function(err) {
        console.log(err);
    })
    
};

exports.setNotificationPrefs = function(req,res) {

    var classID = req.params.classID;
    var userID = req.user.id;
    var prefType = req.body.PrefID;

    db.NotificationPreference.create( {
        ClassId: classID,
        UserId: userID,
        NotificationTypeId: prefType
    }).error(function(err) {
        console.log(err);
    });
    res.end();

};

exports.deleteNotificationPrefs = function(req,res) {

    var classID = req.params.classID;
    var userID = req.user.id;
    var prefType = req.params.prefID;

    db.NotificationPreference.find({
        where: ['"UserId"=? and "ClassId"=? and "NotificationTypeId"=?', userID, classID, prefType]
    }).success(function(pref) {
        if(pref != undefined && pref != null) {
            pref.destroy()
             .error(function(err) {
                console.log(err);
            });
         }

    }).error(function(err) {
        console.log(err);
    });

    res.end();

};

exports.getNotifications = function(req,res) {

    var userID = req.user.id;
    var response = [];

    db.Notification.findAll({
        where: {UserId: userID}
    }).success(function(notifications) {
        for(notify in notifications) {
            response.push( {
                title: notifications[notify].title,
                text: notifications[notify].text,
                typeId: notifications[notify].NotificationTypeId,
                classId: notifications[notify].ClassId 
            });
        }

        res.json(response);

    }).error(function(err) {
        console.log(err);
    })

}

exports.clearNotifications = function(req,res) {

    var userID = req.user.id;
    var classID = req.params.classId;
    var typeID = req.params.typeId;

    db.Notification.findAll({
        where: ['"UserId"=? and "ClassId"=? and "NotificationTypeId"=?',userID, classID, typeID]
    }).success(function(notifications) {
        for(notify in notifications) {
            notifications[notify].destroy().error(function(err) {
                console.log(err);
            });
        }
        res.end();


    }).error(function(err) {
        console.log(err);
    });



}
