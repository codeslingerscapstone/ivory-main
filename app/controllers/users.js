/**
 * Module dependencies.
 */
var db = require('../../config/sequelize');

// Ends the user's session and redirects to the home page
exports.logout = function(req, res) {
    console.log('Logout: { id: ' + req.user.id + ', username: ' + req.user.username + '}');
    req.logout();
    res.redirect('/');
};

// This route is called after passport's authentication middleware, see routes
exports.login = function(req, res) {
    res.redirect('/');
};

// Creates a user and logs them in
exports.create = function(req, res) {
    var message = null;

    var user = db.User.build(req.body);

    user.salt = user.makeSalt();
    user.hashedPassword = user.encryptPassword(req.body.password, user.salt);
    console.log('New User (local) : { id: ' + user.id + ' username: ' + user.username + ' }');

    user.save().success(function(){
      req.login(user, function(err){
        if(err) return next(err);
        res.redirect('/');
      });
    }).error(function(err){
      res.end('Error creating user');
    });
};

exports.userClasses  = function(req, res) {
  var userID = req.user.id;
  var index  = 0;
  var response = [];

  db.ClassUser.findAll({where : { UserId: userID }}).success(function(classUsers){
    for(var i=0; i<classUsers.length; i++) {
      db.Class.find({where: {id: classUsers[i].ClassId}}).success(function(classObj){
        db.Course.find({where: {id: classObj.CourseId}}).success(function(course) {
          var roleNum = classUsers[index].RoleId;
          var roleStr = "" + roleNum;
          response.push({
            courseName: course.name,
            courseNum: course.number,
            courseDep: course.department,
            classId: classObj.id,
            role: roleStr
          });
          index++;

          if(response.length == classUsers.length) {
            res.jsonp(response);
          }

        }).error(function(err) {
          console.log(err);
        });
      }).error(function(err) {
        console.log(err);
      });
    }
  }).error(function(err){
    console.log(err);
  });
};

// Finds a user by id (not used for now, but keeping it around for later)
exports.user = function(req, res, next, id) {
    db.User.find({where : { id: id }}).success(function(user){
      if (!user) return next(new Error('Failed to load User ' + id));
      req.profile = user;
      next();
    }).error(function(err){
      next(err);
    });
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// GET /api/users/me
// Gets information about the current user
exports.me = function(req, res) {
    if (req.user) {
        delete req.user.hashedPassword;
        delete req.user.dataValues.salt;
        res.json(req.user);
    } else {
        res.end('not logged in, dummy');
    }
};

// GET /api/users/:userID
// Gets information about a user
exports.getUser = function (req, res) {
    db.User.find({where: {id: req.params.userID}}).success(function (user) {
        if (user) {
            delete user.dataValues.hashedPassword;
            delete user.dataValues.salt;
            res.json(user);
        } else {
            res.end('that user does not exist');
        }
    }).error(function (err) { console.log(err); res.end('error'); });
};


// POST /api/users/
// Adds a new user (same as user.create, but doesn't log them in)
exports.addUser = function(req, res) {
    var message = null;

    var user = db.User.build(req.body);

    user.salt = user.makeSalt();
    user.hashedPassword = user.encryptPassword(req.body.password, user.salt);
    console.log('New User (local) : { id: ' + user.id + ' username: ' + user.username + ' }');

    user.save().success(function(){
      res.end();
    }).error(function (err) { console.log(err); res.end('error'); });
};

// PUT /api/users/
// Updates a user's information
exports.updateUser = function (req, res) {
    db.User.find(req.body.id).success(function (user) {
        for (field in req.body) {
            user[field] = req.body[field];
        }
    }).error(function (err) { console.log(err); res.end('error'); });
};

// DELETE /api/users/:userID
// Deletes a user
exports.deleteUser = function (req, res) {
    db.User.find(req.params.userID).success(function (user) {
        user.destroy().success(function () {
            res.end();
        }).error(function (err) { console.log(err); res.end('error'); });
    }).error(function (err) { console.log(err); res.end('error'); });
};

exports.allUsers = function(req,res) {

  //removing CourseId association for now
  //can add back later if needed
  var response = [];
  var associationsArr = [];
  var classIds = [];
  //var courseIds = [];
  var index = 0;

  db.User.findAll().success(function(users) {
    for(user in users) {
      db.ClassUser.findAll({where: {UserId: users[user].id}}).success(function(classUsers) {
        
        classIds[index] = [];
        for(cUser in classUsers) {
          classIds[index].push(classUsers[cUser].ClassId);
        }
        
      }).error(function(err) {
        console.log(err);
      }).then(function() {

        associationsArr[index] = {};
        associationsArr[index]["classIds"] = classIds[index];
        //associationsArr[index]["courseIds"] = courseIds[index];

        response.push({
            id: users[index].id,
            name: users[index].name,
            email: users[index].email,
            occupation: users[index].occupation,
            associations: associationsArr[index]
          });
          index++;

      
          if(index == users.length) {
            res.json(response);
          }

      });

  }
      
  }).error(function(err) {
    console.log(err);
  });
}
