var db = require('../../config/sequelize');

exports.getPosts = function(req,res) {

    var classID = req.params.classID;
    var response = [];

    db.Post.findAll({where: {ClassId: classID}}).success(function(posts) {
        for(post in posts) {
            response.push( {
                id: posts[post].id,
                title: posts[post].title,
                text: posts[post].text,
                priority: posts[post].priority,
                timestamp: posts[post].timestamp,
                userID: posts[post].UserId,
                author: posts[post].author
            });
        }

        res.jsonp(response);

    }).error(function(err) {
        console.log(err);
    });

}

exports.newPost = function(req,res) {

    var classID = req.params.classID;

    db.Post.create({
        text: req.body.text,
        title: req.body.title,
        priority: req.body.priority,
        author: req.body.author,
        UserId: req.user.id,
        ClassId: classID
    }).success(function() {
        db.ClassUser.findAll({where: {ClassId: req.params.classID}}).success(function(classusers) {
            for(var user in classusers) {
                if(classusers[user].RoleId == 1) {
                    var notifyText = 'New post in class ' + req.params.classID;
                    var newNotify = db.Notification.build({
                        UserId: classusers[user].UserId,
                        ClassId: parseInt(req.params.classID),
                        NotificationTypeId: 4,
                        title: 'New Post', 
                        text: notifyText,
                        toEmail: true
                    }).save().error(function(err) {
                        console.log(err);
                        res.end();
                    })
                }
            }
            res.end();
        }).error(function(err) {
            console.log(err);
            res.end(err);
        })

        res.end('created successfully');
    }).error(function(err) {
        console.log(err);
        res.end();
    });


}



exports.deletePost = function(req,res) {

    db.Post.find({where: {id: req.params.postID}}).success(function(post) {

        post.destroy()
         .success(function() {
            res.end();
         })
         .error(function(err) {
            console.log(err);
            res.end();
         });

         db.Comment.findAll({where: {PostId: req.params.postID}}).success(function(comments) {

            for(var comment in comments) {
                comments[comment].destroy()
                 .error(function(err) {
                    console.log(err);
                    res.end();
                 })
            }
            res.end();

         }).error(function(err) {
            console.log(err);
            res.end();
         });

    }).error(function(err) {
        console.log(err);
        res.end();
    });
};