var db = require('../../config/sequelize');



exports.getGrades = function(req,res) {

    var userID = req.user.id;
    var classID = req.params.classId;
    var response = [];
    var index = 0;
    var count = 0;

    db.Assignment.findAll({where: {ClassId: classID}}).success(function(assignments) {
        if(assignments.length == 0) {
            res.end();
        }
        console.log(assignments.length);
        for(assign in assignments) {
            db.AssignmentAttempt.find({
                where: ['"UserId"=? and "AssignmentId"=?',userID, assignments[assign].id] 
            }).success(function(attempt) {

                count++;
                if(attempt != null) {
                    response.push({
                        grade: attempt.grade,
                        title: assignments[index].title, 
                        due: assignments[index].due,
                        assignmentID: attempt.AssignmentId,
                        classID: classID
                    });
                    index++;
                }

                    if(count == assignments.length) {
                        res.jsonp(response);
                    }
                
            }).error(function(err) {
                console.log(err);
            });
        }


    }).error(function(err) {
        console.log(err);
    });

}


exports.getTeacherGrades = function(req,res) {

    var userID = req.params.userId;
    var classID = req.params.classId;
    var userName;
    var response = [];
    var index = 0;
    var count = 0;

    db.User.find({where: {id: userID}}).success(function(user) {
        userName = user.name;
    }).error(function(err) {
        console.log(err);
    }).then(function() {

        db.Assignment.findAll({where: {ClassId: classID}}).success(function(assignments) {
            if(assignments.length == 0) {
                res.end();
            }
            console.log(assignments.length);
            for(assign in assignments) {
                db.AssignmentAttempt.find({
                    where: ['"UserId"=? and "AssignmentId"=?',userID, assignments[assign].id] 
                }).success(function(attempt) {

                    count++;
                    if(attempt != null) {
                        response.push({
                            name: userName,
                            grade: attempt.grade,
                            title: assignments[index].title, 
                            due: assignments[index].due,
                            assignmentID: attempt.AssignmentId,
                            classID: classID,
                            userID: userID,
                            doc: attempt.DocumentId
                        });
                        index++;
                    }
                    else {
                        response.push({
                            name: userName,
                            grade: "N/A",
                            title: assignments[index].title, 
                            due: assignments[index].due,
                            assignmentID: assignments[index].id,
                            classID: classID,
                            userID: userID,
                            doc: null

                        });
                        index++;
                    }

                        if(count == assignments.length) {
                            res.jsonp(response);
                        }
                    
                }).error(function(err) {
                    console.log(err);
                });
            }


        }).error(function(err) {
            console.log(err);
        });
    });

}

exports.updateGrade = function(req,res) {

    var userID = req.params.userId;
    var assignID = req.params.assignId;
    var grade = req.params.grade;
    var classID = req.params.classId;

    db.AssignmentAttempt.find({
        where: ['"UserId"=? and "AssignmentId"=?',userID, assignID]
    }).success(function(attempt) {
        if(attempt != null) {
            attempt.updateAttributes({
                grade: grade
            }).success(function() {
                res.end();
            }).error(function(err) {
                console.log(err);
            });
        }
        else {
            var newAssign= db.AssignmentAttempt.build({
                grade: grade,
                UserId: userID,
                AssignmentId: assignID
            }).success(function() {
                res.end();
            });

            newAssign.save().error(function(err) {
                console.log(err);
            });
        }


    }).error(function(err) {
        console.log(err);
    }).then(function() {
        
    });

    var notifyText = 'Grades in class ' + classID + ' updated';

    var newNotify = db.Notification.build({
        UserId: userID,
        ClassId: classID,
        NotificationTypeId: 1,
        title: 'Updated Grades', 
        text: notifyText,
        toEmail: true
    }).save().error(function(err) {
        console.log(err);
    })

}



