var db = require('../../config/sequelize');

exports.getDoc = function(req,res) {

    var documentID = req.params.documentId;
    var response = {};

    db.Document.find({where: {id: documentID}}).success(function(doc) {
        response = {
            id: doc.id,
            text: doc.text,
            filePath: doc.filePath
        };

        res.jsonp(response);

    }).error(function(err) {
        console.log(err);
    });

};
