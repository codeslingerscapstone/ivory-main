module.exports = function(sequelize, Sequelize) {

	var TeacherPolicy = sequelize.define('TeacherPolicy', {

		},

		{
			timestamps: false,
			associate: function(models) {
				TeacherPolicy.belongsTo(models.Policy);
				TeacherPolicy.belongsTo(models.User);
			}
		}
	);

	return TeacherPolicy;
}