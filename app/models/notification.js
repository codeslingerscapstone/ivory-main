module.exports = function(sequelize, Sequelize) {
	var Notification = sequelize.define('Notification', {
			title: {type: Sequelize.STRING, allowNull:false},
			text: {type: Sequelize.TEXT},
			toEmail: {type: Sequelize.BOOLEAN}
		
		},

		{
			timestamps: false,
			associate: function(models) {
				Notification.belongsTo(models.NotificationType);
				Notification.belongsTo(models.Class);
				Notification.belongsTo(models.User);

			}
		}
	);
	
	return Notification;
}