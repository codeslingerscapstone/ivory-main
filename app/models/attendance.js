module.exports = function(sequelize, Sequelize) {

	var Attendance = sequelize.define('Attendance', {
			date: {
				type: Sequelize.STRING,
				validate: {
					notNull: true,
					notEmpty: true,
					is:["^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$",'i']
				}},
			status: {
				type:Sequelize.ENUM,
				
				/*
					P = Present
					EL = Excused Late
					UL = Unexcused Late
					EA = Excused Absent
					UA = Unexcused Absent 
				*/
				values: ['P', 'EL', 'UL', 'EA', 'UA']
			}
		}, 

		{
			timestamps: false,
			associate: function(models) {
				Attendance.belongsTo(models.Class);
				Attendance.belongsTo(models.User);
			}
		}
	);

	return Attendance;
}