module.exports = function(sequelize, Sequelize) {

	var Class = sequelize.define('Class', {
			closed: {type: Sequelize.BOOLEAN, allowNull:false},
			time: {
				type: Sequelize.STRING,
				validate: {
					notNull: true,
					notEmpty: true,
					//isTime custom
				}
			},
			days: {type: Sequelize.STRING, allowNull:false},
			location: {type: Sequelize.STRING, allowNull:false},
			numberOfSeats: {type: Sequelize.INTEGER}
		},

		{
			timestamps: false,
			associate: function(models) {
				Class.hasMany(models.Assignment);
				Class.hasMany(models.Quiz);
				Class.hasMany(models.Post);

				Class.hasMany(models.NotificationPreference);
				Class.hasMany(models.ClassUser);
				Class.hasMany(models.Attendance);
				Class.hasOne(models.ClassPolicy);
				Class.hasOne(models.Syllabus);
				Class.belongsTo(models.Course);
			}
		}

	);

	return Class;
}