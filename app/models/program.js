module.exports = function(sequelize, Sequelize) {

	var Program = sequelize.define('Program', {
			name: {type:Sequelize.STRING, allowNull: false},
			language: {type:Sequelize.STRING, allowNull: false},    //also need to provide compile command
			checkboxes: {type:Sequelize.STRING, allowNull: false},
			memory: {type: Sequelize.STRING, allowNull: false}, //in megabytes      halfgig - 2 gigs
			diskSpace: {type: Sequelize.STRING, allowNull: false}, //in megabytes   256-1024
			runLimit: {type: Sequelize.STRING, allowNull: false},
			accuracy: {type:Sequelize.STRING, allowNull: false},    //"low" or "high"
			input: {type: Sequelize.BOOLEAN},
			testFile: {type: Sequelize.INTEGER},  //foreign key reference to Document
			outputFile: {type: Sequelize.INTEGER} //foreign key reference to Document
		},

		{
			timestamps: false,
			associate: function(models) {
				Program.hasOne(models.Assignment);
			}
		}
	);

	return Program;

};
