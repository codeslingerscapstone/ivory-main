module.exports = function(sequelize, Sequelize) {

	var StudentAnswer = sequelize.define('StudentAnswer', {

		},

		{
			timestamps: false,
			associate: function(models) {
				StudentAnswer.belongsTo(models.Question);
				StudentAnswer.belongsTo(models.Answer);
				StudentAnswer.belongsTo(models.User);	
			}
			
		}
	);

	return StudentAnswer;
}