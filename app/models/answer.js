module.exports = function(sequelize, Sequelize) {

	var Answer = sequelize.define('Answer', {
			text: {type: Sequelize.TEXT, allowNull: true},
			correct: {type: Sequelize.BOOLEAN}
		},

		{
			timestamps: false,
			associate: function(models) {
				Answer.belongsTo(models.Question);
				Answer.hasMany(models.StudentAnswer);
			}
		}
	);

	return Answer;
}