module.exports = function(sequelize, Sequelize) {

	var Role = sequelize.define('Role', {
			name: {type: Sequelize.STRING, allowNull: false, unique: true}
		}, 

		{
			timestamps: false,
			associate: function(models) {
				Role.hasMany(models.ClassUser);
			}
		}
	);

	return Role;
}