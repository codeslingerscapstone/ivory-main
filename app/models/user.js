var crypto = require('crypto');

module.exports = function(sequelize, Sequelize) {
	
	var User = sequelize.define('User', {
			name: {
				type:Sequelize.STRING, 
				validate: {
					//is: ["regex",'i'], 
					notNull: true,
					notEmpty: true

				}
			},
			username: {
				type:Sequelize.STRING,
				validate: {
					//is: ["regex",'i'],  //set some regex for usernames
					notNull: true,
					notEmpty: true,
				}
			},
			
			hashedPassword: Sequelize.STRING,
			salt: Sequelize.STRING,

			email: {
				type:Sequelize.STRING,
				validate: {
					notNull: true,
					notEmpty: true,
					isEmail: true
				}
			},

			occupation: {type: Sequelize.STRING}
		},

		{
			timestamps: false,

			instanceMethods: {
				makeSalt: function() {
					return crypto.randomBytes(16).toString('base64');
				},
				authenticate: function(plainText){
					return this.encryptPassword(plainText, this.salt) === this.hashedPassword;
				},
				encryptPassword: function(password, salt) {
					if (!password || !salt) return '';
					salt = new Buffer(salt, 'base64');
					return crypto.pbkdf2Sync(password, salt, 10000, 64).toString('base64');
				}
			},


			associate: function(models) {
				User.hasMany(models.Notification);
				User.hasMany(models.Comment);
				User.hasMany(models.Post);

				User.hasOne(models.ClassUser);
				User.hasOne(models.Attendance);
				User.hasMany(models.NotificationPreference);
				User.hasOne(models.TeacherPolicy); 
				User.hasMany(models.AssignmentAttempt);
				User.hasMany(models.StudentUpload);
				User.hasMany(models.QuizAttempt);
				User.hasMany(models.StudentAnswer);
			}
		}
	);

	return User;
};