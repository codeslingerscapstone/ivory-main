module.exports = function(sequelize, Sequelize) {

	var ClassPolicy = sequelize.define('ClassPolicy', {

		},

		{
			timestamps: false,
			associate: function(models) {
				ClassPolicy.belongsTo(models.Class);
				ClassPolicy.belongsTo(models.Policy);
			}
		}
	);

	return ClassPolicy;
}