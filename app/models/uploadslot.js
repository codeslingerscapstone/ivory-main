module.exports = function(sequelize, Sequelize) {

	var UploadSlot = sequelize.define('UploadSlot', {
			fileName: {type: Sequelize.STRING},
			type: {type: Sequelize.STRING},
			description: {type: Sequelize.STRING}
		},

		{
			timestamps: false,
			associate: function(models) {
				UploadSlot.hasMany(models.StudentUpload);
				UploadSlot.belongsTo(models.Assignment);
			}
		}
	);

	return UploadSlot;

}