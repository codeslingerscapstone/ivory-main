module.exports = function(sequelize, Sequelize) {
	
	var NotificationPreference = sequelize.define('NotificationPreference', {

		}, 

		{
			timestamps: false,
			associate: function(models) {
				NotificationPreference.belongsTo(models.Class);
				NotificationPreference.belongsTo(models.User);
				NotificationPreference.belongsTo(models.NotificationType);
			}
		}
	);

	return NotificationPreference;

}