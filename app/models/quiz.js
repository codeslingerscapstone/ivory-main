module.exports = function(sequelize, Sequelize) {

	var Quiz = sequelize.define('Quiz', {
			created: {type: Sequelize.DATE, defaultValue: Sequelize.NOW},
			due: {type: Sequelize.DATE, allowNull: false},
			attempts: {type: Sequelize.INTEGER}
		},

		{
			timestamps: false,
			associate: function(models) {
				Quiz.hasMany(models.Question);
				Quiz.belongsTo(models.Class);
			}
		}
	);

	return Quiz;
}