module.exports = function(sequelize, Sequelize) {

	var Syllabus = sequelize.define('Syllabus', {

		}, 

		{
			timestamps: false,
			associate: function(models) {
				Syllabus.belongsTo(models.Document);
				Syllabus.belongsTo(models.Class);
			}
		}
	);

	return Syllabus;
}