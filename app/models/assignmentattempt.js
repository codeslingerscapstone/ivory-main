module.exports = function(sequelize, Sequelize) {

	var AssignmentAttempt = sequelize.define('AssignmentAttempt', {
			timestamp: {type: Sequelize.DATE, defaultValue:Sequelize.NOW},
			grade: {type:Sequelize.INTEGER, allowNull:true},
		}, 

		{
			timestamps: false,
			associate: function(models) {
				AssignmentAttempt.belongsTo(models.User);
				AssignmentAttempt.belongsTo(models.Assignment);
				AssignmentAttempt.belongsTo(models.Document); //maybe?

			}
		}
	);

	return AssignmentAttempt;
}