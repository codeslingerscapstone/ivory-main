module.exports = function(sequelize, Sequelize) {

	var Policy = sequelize.define('Policy', {
			title: {type: Sequelize.STRING, allowNull:false},
			text: {type: Sequelize.TEXT},
		},

		{
			timestamps: false,
			associate: function(models) {
				Policy.hasOne(models.ClassPolicy);
				Policy.hasOne(models.TeacherPolicy);
			}
		}
	);

	return Policy;

}