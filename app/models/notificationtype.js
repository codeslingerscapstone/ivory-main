module.exports = function(sequelize, Sequelize) {

	var NotificationType = sequelize.define('NotificationType', 
		{
			name: {type: Sequelize.STRING, allowNull:false, unique:true}
		},

		{
			timestamps: false,
			associate: function(models) {
				NotificationType.hasMany(models.Notification);
			}

		}
	);

	return NotificationType;
}