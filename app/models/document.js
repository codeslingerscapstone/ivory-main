module.exports = function(sequelize, Sequelize) {

	var Document = sequelize.define('Document', {
			created: {type: Sequelize.DATE, defaultValue:Sequelize.NOW},
			text: {
				type:Sequelize.STRING(50000)
			},
			
			filePath: {
				type:Sequelize.STRING
			}
		},

		{
			timestamps: false,
			associate: function(models) {
				Document.hasOne(models.Syllabus);
				Document.hasOne(models.AssignmentPrompt);
				Document.hasOne(models.StudentUpload);
			}
		}
	);

	return Document;
}