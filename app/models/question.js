module.exports = function(sequelize, Sequelize) {
	
	var Question = sequelize.define('Question', {
			query: {type: Sequelize.STRING, allowNull: false},
			value: {type: Sequelize.INTEGER, allowNull:false}
		},

		{
			timestamps: false,
			associate: function(models) {
				Question.hasMany(models.Answer);
				Question.hasMany(models.StudentAnswer);
				Question.belongsTo(models.Quiz);
			}
		}
	);

	return Question;
}