module.exports = function(sequelize, Sequelize) {

	var Execution = sequelize.define('Execution', {
			timeSubmitted: {type: Sequelize.DATE, defaultValue:Sequelize.NOW},
			outputFile: {type: Sequelize.INTEGER},
			status: {
				type:Sequelize.ENUM,
				
				/*
					0 = not done
					1 = done
					2 = notified
				*/
				values: ['0', '1', '2']
			}
		},

		{
			timestamps: false,
			associate: function(models) {
				Execution.belongsTo(models.Program);
				Execution.belongsTo(models.User);
			}
		}
	);

	return Execution;

}