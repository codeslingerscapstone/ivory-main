module.exports = function(sequelize, Sequelize) {

	var ClassUser = sequelize.define('ClassUser', {

		},

		{
			timestamps: false,
			associate: function(models) {
				ClassUser.belongsTo(models.Role);
				ClassUser.belongsTo(models.Class);
				ClassUser.belongsTo(models.User);
			}
		}
	);

	return ClassUser;

}