module.exports = function(sequelize, Sequelize) {

	var Comment = sequelize.define('Comment', {
			text: {type:Sequelize.STRING},
			timestamp: {type:Sequelize.DATE, defaultValue: Sequelize.NOW},
			author: {type:Sequelize.STRING}
		},

		{
			timestamps: false,
			associate: function(models) {
				Comment.belongsTo(models.Post);
				Comment.belongsTo(models.User);
			}
		}
	);

	return Comment;
}