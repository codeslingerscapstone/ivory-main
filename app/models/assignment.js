module.exports = function(sequelize, Sequelize) {

	var Assignment = sequelize.define('Assignment', {
			title: {type:Sequelize.STRING},
			created: {type: Sequelize.DATE, defaultValue:Sequelize.NOW},
			due: {type: Sequelize.STRING, allowNull: false},
			tokenNum: {type: Sequelize.INTEGER, allowNull: false},
			tokenType: {
				type: Sequelize.ENUM,
				values: ["weekly", "daily", "hourly", "total"]
			},
			submission: {type: Sequelize.BOOLEAN}
		},

		{
			timestamps: false,
			associate: function(models) {
				Assignment.hasOne(models.UploadSlot);
				Assignment.hasMany(models.AssignmentAttempt);
				Assignment.hasOne(models.AssignmentPrompt);
				Assignment.belongsTo(models.Class);
				Assignment.belongsTo(models.Program);
			}
		}

	);

	return Assignment;

}