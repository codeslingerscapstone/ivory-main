module.exports = function(sequelize, Sequelize) {

	var StudentUpload = sequelize.define('StudentUpload', {

		},

		{
			timestamps: false,
			associate: function(models) {
				StudentUpload.belongsTo(models.Document);
				StudentUpload.belongsTo(models.User);
				StudentUpload.belongsTo(models.UploadSlot);
			}
		}
	);

	return StudentUpload;
}