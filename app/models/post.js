module.exports = function(sequelize, Sequelize) {
	var Post = sequelize.define('Post', {
			text: {type:Sequelize.STRING(50000)},
			title: {type:Sequelize.STRING},
			author: {type:Sequelize.STRING},
			priority: {
				type:Sequelize.ENUM,
				values: ['low','medium','high']

			},
			timestamp: {type:Sequelize.DATE, defaultValue: Sequelize.NOW}
		},

		{
			timestamps: false,
			associate: function(models) {
				Post.hasMany(models.Comment);
				Post.belongsTo(models.User);
				Post.belongsTo(models.Class)
			}
		}
	);

	return Post;
}