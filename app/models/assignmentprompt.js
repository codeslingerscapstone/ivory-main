module.exports = function(sequelize, Sequelize) {

	var AssignmentPrompt = sequelize.define('AssignmentPrompt', {

		},

		{
			timestamps: false,
			associate: function(models) {
				AssignmentPrompt.belongsTo(models.Document);
				AssignmentPrompt.belongsTo(models.Assignment);

			}
		}
	);

	return AssignmentPrompt;
}