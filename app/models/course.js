module.exports = function(sequelize, Sequelize) {

	var Course = sequelize.define('Course', {
			department: {
				type:Sequelize.STRING,
				validate: {
					notNull: true,
					notEmpty: true,
					len:[3,3]    //Baylor specific, constants file?
				}
			}, 
			number: {
				type:Sequelize.STRING,
				validate: {
					notNull: true,
					notEmpty: true,
					len: [4,4]  //Baylor specific, constants file?
				}
			},
			name: {type:Sequelize.STRING, allowNull:false}
		},

		{
			timestamps: false,
			associate: function(models) {
				Course.hasMany(models.Class);
			}
		}
	);

	return Course;
}