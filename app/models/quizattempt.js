module.exports = function(sequelize, Sequelize) {

	var QuizAttempt = sequelize.define('QuizAttempt', {
			timestamp: {type:Sequelize.DATE, defaultValue: Sequelize.NOW}		
		},

		{
			timestamps: false,
			associate: function(models) {
				QuizAttempt.belongsTo(models.User);
				QuizAttempt.belongsTo(models.StudentAnswer);
			}
		}
	);

	return QuizAttempt;
}